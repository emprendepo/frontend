import { validateEmail } from "./";
import { validate, formatRut, clean } from "./rut";

export const EmailValid = {
  min: 7,
  max: 38,
  validations: [
    {
      field: false,
      validateFunction: (val, current) => {
        return validateEmail(current);
      },
      error: `Ingrese un email válido`,
    },
  ],
};

export const RutValid = {
  min: 5,
  validations: [
    {
      field: false,
      validateFunction: (val, current) => {
        return validate(clean(current));
      },
      error: "Rut , invalido",
    },
  ],
};

export const DescripcionValid = {
  min: 10,
  max: 50,
};

export const ValorValid = {
  min: 3,
  validations: [
    {
      field: false,
      validateFunction: (val, current) => {
        return current > 0;
      },
      error: `El numero debe ser mayor a 0`,
    },
  ],
};

export const TelefonosValid = {
  min: 8,
  max: 9,
};

export const handleValidation = (keyName) =>
  ({
    email: EmailValid,
    rut: RutValid,
    descripcion: DescripcionValid,
    valor: ValorValid,
    telefono: TelefonosValid,
    celular: TelefonosValid,
  }[keyName] || null);

export const handleValidationValue = (value, keyName) =>
  ({
    rut: formatRut(value),
  }[keyName] || value);

export function handleValue(value) {
  if (Array.isArray(value)) {
    return value?.map(({ id }) => id) ?? [];
  }
  return value;
}
