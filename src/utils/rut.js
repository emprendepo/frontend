export function clean(rut) {
  return typeof rut === `string`
    ? rut.replace(/^0+|[^0-9kK]+/g, ``).toUpperCase()
    : ``;
}

export function formatRut(rut) {
  rut = clean(rut);
  let result = ``;
  if (rut.length > 0) {
    result = `${rut.slice(-4, -1)}-${rut.substr(rut.length - 1)}`;
  }
  for (let i = 4; i < rut.length; i += 3) {
    result = `${rut.slice(-3 - i, -i)}.${result}`;
  }
  return result;
}

export function servicioRut(rut) {
  rut = clean(rut);
  return rut.slice(0, rut.length - 1);
}
export function dgv(T) {
  let M = 0,
    S = 1;
  for (; T; T = Math.floor(T / 10)) S = (S + (T % 10) * (9 - (M++ % 6))) % 11;
  return S ? S - 1 : `k`;
}
export function validate(rut) {
  if (
    [
      "19",
      "111111111",
      "222222222",
      "555555555",
      "444444444",
      "333333333",
    ].includes(rut)
  ) {
    return false;
  }
  if (typeof rut !== `string`) {
    return false;
  }
  if (!/^0*(\d{1,3}(\.?\d{3})*)-?([\dkK])$/.test(rut)) {
    return false;
  }

  rut = clean(rut);

  let t = parseInt(rut.slice(0, -1), 10);
  let m = 0;
  let s = 1;

  while (t > 0) {
    s = (s + (t % 10) * (9 - (m++ % 6))) % 11;
    t = Math.floor(t / 10);
  }

  const v = s > 0 ? `${s - 1}` : `k`;
  return v.toLowerCase() === rut.slice(-1).toLowerCase();
}
