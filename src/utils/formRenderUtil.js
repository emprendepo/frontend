import React from "react";
import InputAdornment from "@material-ui/core/InputAdornment";

export const renderComponentWithProps = (
  field,
  onChangeForm,
  component,
  keyName,
  Component,
  itemId
) => (
  <Component
    {...field}
    itemId={itemId}
    keyName={field?.name}
    error={field.error}
    label={field.label}
    placeholder={field.placeholder}
    onChange={({ name, value, directComponentError }) =>
      onChangeForm({
        name,
        value,
        directComponentError,
      })
    }
    size="medium"
    variant="outlined"
    type={field.type}
    max={field.max ? field.max : ""}
    fullWidth={field.fullWidth ? field.fullWidth : true}
    name={keyName}
    disabled={field.disabled}
    native={field.native}
    InputProps={{
      startAdornment: field.adornment ? (
        <InputAdornment position={field.adornment.position}>
          {field.adornment.label}
        </InputAdornment>
      ) : (
        ""
      ),
    }}
    inputProps={{
      maxLength: field.maxLength ? field.maxLength : "",
      minLength: field.minLength ? field.minLength : "",
    }}
    value={field.value}
    isRequired={field.inputRequired}
    props={field.props}
    rows={field.rows}
    rowsMax={field.rowsMax}
  />
);
