import React from "react";
import { withStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import MuiDialogTitle from "@material-ui/core/DialogTitle";
import MuiDialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";
import Grid from "@material-ui/core/Grid";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import Typography from "@material-ui/core/Typography";
const styles = (theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(2),
  },
  title: {
    padding: theme.spacing(2, 1),
  },
  closeButton: {
    position: "absolute",
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },
});

const DialogTitle = withStyles(styles)((props) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      {onClose ? (
        <IconButton
          aria-label="close"
          className={classes.closeButton}
          onClick={onClose}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles((theme) => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiDialogContent);

function CustomizedDialogs({
  classes,
  openModal = false,
  title,
  showXButton = true,
  disableBackdropClick = false,
  disableEscapeKeyDown = false,
  onClose,
  children = null,
  showAction = false,
  maxWidth = "md",
  textActionOk = "Subscribe",
  textActionCancel = "Cancelar",
  isVisibleActionOk = true,
  isVisibleActionCancel = true,
}) {
  return (
    <Dialog
      onClose={onClose}
      fullWidth={true}
      maxWidth={maxWidth}
      aria-labelledby={title}
      open={openModal}
      disableBackdropClick={disableBackdropClick}
      disableEscapeKeyDown={disableEscapeKeyDown}
    >
      {showXButton && (
        <DialogTitle id="customized-dialog-title" onClose={onClose} />
      )}
      <Grid container justify="center">
        <Typography variant="h2" align="center" className={classes.title}>
          {title}
        </Typography>
      </Grid>
      <DialogContent>
        <Grid container alignItems="center" direction="column" spacing={2}>
          {children}
        </Grid>
      </DialogContent>
      {showAction && (
        <DialogActions>
          {isVisibleActionCancel && (
            <Button color="primary">{textActionCancel}</Button>
          )}
          {isVisibleActionOk && (
            <Button color="primary" onClick={showAction}>
              {textActionOk}
            </Button>
          )}
        </DialogActions>
      )}
    </Dialog>
  );
}

export default withStyles(styles)(CustomizedDialogs);
