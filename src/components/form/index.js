import React from "react";
import ButtonInfo from "../ButtonInfo";
import {
  TextInput,
  Checkbox,
  Select,
  TextArea,
  FileUpload,
  ButtonAdd,
} from "./components";
import { CustomText, LinkButton, CustomHeader } from "./custom";
import { Grid, Typography, makeStyles } from "@material-ui/core";
import commonStyles from "../../utils/commonInlineStyles";
import { useHistory } from "react-router-dom";
import { renderComponentWithProps } from "../../utils/formRenderUtil";
import { handleValidation } from "../../utils/validations";

const components = {
  TextInput,
  Checkbox,
  Select,
  CustomText,
  TextArea,
  FileUpload,
  CustomHeader,
  ButtonAdd,
};
function Form({
  itemId = null,
  form,
  loading,
  onChangeForm,
  submitForm,
  isValid,
  formName,
  formSubName,
  showBackButton = false,
  hideSubmitButton = false,
  formError = false,
  textSubmitForm = `Enviar`,
}) {
  const classes = styles();
  const formKeys = Object.keys(form);
  const history = useHistory();
  return (
    <Grid container className={classes.container}>
      {showBackButton && (
        <LinkButton
          onClick={() => history.goBack()}
          buttonText="Volver atrás"
        />
      )}
      <Grid container item justify="center" alignContent="center">
        <Grid item>
          <Typography
            variant="h1"
            align="center"
            dangerouslySetInnerHTML={{ __html: formName }}
          ></Typography>
          {formSubName && (
            <Typography variant="h6" align="center">
              {formSubName}
            </Typography>
          )}
        </Grid>
      </Grid>
      <Grid container item className={classes.formContainer}>
        {formKeys.map((keyName, index) => {
          const field = form[keyName];
          if (field.hide) {
            return false;
          }
          if (field.groupWithPast) {
            return false;
          }
          field["rules"] =
            field?.rules ?? handleValidation(field?.name) ?? null;

          const component = field.inputType ? field.inputType : field.component;
          const Component = components[component];
          const ComponentToRender = renderComponentWithProps(
            field,
            onChangeForm,
            component,
            keyName,
            Component,
            itemId
          );
          return (
            <Grid item sm={12} key={index} className={classes.item}>
              {ComponentToRender}
            </Grid>
          );
        })}
        {!hideSubmitButton && (
          <Grid container justify="center" alignItems="center">
            <ButtonInfo
              fullWidth
              text={textSubmitForm}
              size="large"
              color="secondary"
              variant="outlined"
              handleOnClick={submitForm}
              className={classes.button}
              isDisabled={!isValid || loading}
              isLoading={loading}
            />
          </Grid>
        )}
        {formError && formError.length > 0 && (
          <Grid container item justify="center">
            <Grid item>
              <Typography className={classes.error}>{formError}</Typography>
            </Grid>
          </Grid>
        )}
      </Grid>
    </Grid>
  );
}

const styles = makeStyles((theme) => ({
  ...commonStyles,
  formContainer: {
    // background: "#f6f7fb",
  },
  button: {
    margin: 10,
  },
  headerTitle: {
    width: "100%",
    color: "white",
  },
  formBody: {
    padding: "15px",
  },
  inputHeader: {
    padding: "10px",
  },
  item: {
    padding: `${theme.spacing(2)}px ${theme.spacing(2)}px 0px ${theme.spacing(
      2
    )}px`,
    width: "100%",
    [theme.breakpoints.down("sm")]: {
      padding: `${theme.spacing(2)}px ${theme.spacing(1)}px ${theme.spacing(
        2
      )}px ${theme.spacing(1)}px`,
    },
  },
  nameContainer: {
    padding: theme.spacing(2),
  },
  container: {
    padding: theme.spacing(2),
    [theme.breakpoints.down("xs")]: {
      padding: theme.spacing(2, 0),
    },
  },
  error: {
    textAlign: `center`,
    color: theme.palette.error.main,
    lineHeight: `1.6em`,
    fontSize: `1.4em`,
    marginTop: theme.spacing(2),
  },
}));

export default Form;
