import React, { useState } from "react";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import { withStyles } from "@material-ui/core/styles";
import HelpOutlineOutlinedIcon from "@material-ui/icons/HelpOutlineOutlined";
import CheckboxWrapper from "./Checkbox";
import { Dialog } from "../../";

const styles = ({
  spacing,
  breakpoints,
  fontSizes,
  palette: { gray, error },
  fontWeights,
}) => ({
  requiredDot: {
    color: `#0B2D48`,
    fontSize: `1em`,
    marginLeft: 2,
    top: 0,
  },
  opcional: {
    fontSize: `1.4rem`,
    fontFamily: `Roboto`,
    fontWeigth: 100,
    fontStyle: `italic`,
    marginLeft: 6,
  },
  paddingRight: {
    paddingRight: spacing(1),
    textAlign: `right`,
    [breakpoints.down(`sm`)]: {
      textAlign: `left`,
    },
  },
  error: {
    textAlign: `right`,
    color: error.main,
    lineHeight: `1.6em`,
    fontSize: `14px`,
  },
  errorColor: {
    color: error.main,
  },
  checkbox: {
    color: gray.main,
    marginTop: `5px`,
  },
  positionLabelWrapper: {
    flexDirection: `row-reverse`,
    justifyContent: `flex-end`,
    alignItems: `center`,
    // marginBottom: spacing(2),
  },
  positionLabelWrapperPadding: {
    paddingLeft: spacing(3),
    paddingRight: spacing(3),
    marginBottom: spacing(2),
  },
  disabled: {
    opacity: 0.6,
  },
  positionLabel: {
    flexBasis: `auto`,
  },
  subLabel: {
    fontSize: fontSizes[0],
    lineHeight: `1rem`,
    fontWeight: fontWeights[0],
  },
  check: {
    margin: 0,
    padding: 0,
    justifyContent: `flex-start`,
  },
  label: {
    fontSize: `13px`,
    fontWeight: `Bold !important`,
    color: "black !important",
  },
  borderTop: {
    borderTop: `1px solid ${gray.main}`,
    paddingTop: spacing(3),
  },
  cursorPointer: {
    "&:hover": {
      cursor: "pointer",
    },
  },
});

const Label = ({
  children,
  error, //Puede ser boolean o string
  label,
  subLabel = ``,
  isRequired = false,
  classes,
  disabled = false,
  positionLabel = `left`,
  xs,
  md,
  check = null,
  haveCheck = false,
  callbacks,
  withPadding = false,
  flexDirection = `row`,
  borderTop = false,
  inlineLabel = false,
  helpAdorment = false,
  HelpComponent = false,
  footer = ``,
  fullWidth = false,
  ...rest
}) => {
  const [dialogHelp, setDialogHelp] = useState({
    open: false,
    title: "",
    component: HelpComponent ? <HelpComponent /> : <div></div>,
  });

  const dialogHelpOpen = () => {
    setDialogHelp({
      ...dialogHelp,
      open: true,
    });
  };

  return (
    <Grid
      item
      xs={xs}
      md={md}
      container
      direction="row"
      alignContent="flex-start"
      className={`${error ? `has-warning` : ``} ${
        borderTop ? classes.borderTop : ``
      }`}
    >
      <React.Fragment>
        <Grid
          container
          xs={12}
          item
          spacing={inlineLabel ? 4 : 0}
          justify={"center"}
          alignItems={"center"}
        >
          {label && (
            <Grid item md={3}>
              <Typography
                variant="h6"
                color="primary"
                className={`${error ? classes.errorColor : ``} ${
                  classes.label
                }`}
              >
                {label}
                {isRequired && "(*)"}
              </Typography>
            </Grid>
          )}
          {helpAdorment && (
            <>
              <Grid
                item
                xs={1}
                md={1}
                className={classes.cursorPointer}
                onClick={dialogHelpOpen}
              >
                <HelpOutlineOutlinedIcon color="secondary" />
              </Grid>
            </>
          )}
          <Grid
            item
            xs={flexDirection === `column` ? 3 : inlineLabel ? 6 : 12}
            md={8}
            className={`${
              positionLabel === `right` ? classes.positionLabel : ``
            }`}
          >
            {subLabel && (
              <Typography variant="h6" className={classes.subLabel}>
                {subLabel}
              </Typography>
            )}
            {children}
            {footer && (
              <Typography variant="h6" className={classes.subLabel}>
                {footer}
              </Typography>
            )}
            {check && (
              <CheckboxWrapper
                haveCheck={true}
                className={classes.check}
                callbacks={callbacks}
                {...check}
              />
            )}
            {error && (
              <Typography className={classes.error}>{error}</Typography>
            )}
          </Grid>
        </Grid>
        {helpAdorment && (
          <Dialog
            openModal={dialogHelp.open}
            onClose={() => setDialogHelp({ ...dialogHelp, open: false })}
            title={dialogHelp.title}
            childComponent={dialogHelp.component}
            showBack={true}
            textButton={"Entendido"}
            customActionButton={"close"}
          />
        )}
      </React.Fragment>
    </Grid>
  );
};

export default withStyles(styles)(Label);
