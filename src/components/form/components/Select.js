import React from "react";

import Select from "@material-ui/core/Select";

import { makeStyles } from "@material-ui/core/styles";

import InputWrapper from "./InputWrapper";

const useStyles = makeStyles(({ palette }) => ({
  chips: {
    display: `flex`,
    flexWrap: `wrap`,
  },
  chip: {
    margin: 2,
  },
  check: {
    fill: palette.success.main,
  },
  openList: {
    border: `1px solid ${palette.primary.dark}`,
    borderRadius: `5px`,
    maxHeight: `12rem`,
    minHeight: `12rem`,
    overflow: `hidden`,
    overflowY: `scroll`,
  },
  emptyText: {
    textAlign: `center`,
  },
  filterInput: {
    background: `white`,
    border: `none`,
    borderBottom: `3px solid #dcdde5`,
  },
}));

const SelectWrapper = ({
  id,
  label,
  error,
  isRequired = false,
  disabled = false,
  onChange,
  classes: classesParent,
  items = [],
  value,
  name,
  allowClear = false,
  hasSearch = true,
  native = false,
  mode = `default`,
  openList = false,
  placeholder = `Seleccione`,
  align = `left`,
  md = 12,
  xs = 12,
  variant = `standard`,
  withSelectOption = true,
  isValid,
  size = "medium",
  ...rest
}) => {
  const classes = useStyles();
  const iconName =
    isValid && value !== -1 && value !== null ? `check` : `select`;

  let props = {
    name,
    classes: classesParent,
    variant,
    mode, //MULTI DEFAULT ETC...,
    placeholder,
    value: value || [],
    disabled,
    allowClear,
    onChange: ({ target: { value, name } }) => {
      onChange({ name, value, id });
    },
    showSearch: hasSearch,
    filterOption: (input, option) =>
      hasSearch
        ? option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
        : null,
    //    IconComponent: Icon[iconName],
    inputProps: {
      classes: {
        icon: iconName === `check` ? classes.check : ``,
      },
    },
  };

  return (
    <InputWrapper
      label={label}
      error={error}
      isRequired={isRequired}
      align={align}
      md={md}
      xs={xs}
      disabled={disabled}
      {...rest}
    >
      <Select
        fullWidth
        native
        className={variant === `filled` ? classes.filterInput : ``}
        {...props}
      >
        <option value="">{placeholder}</option>
        {items.map((item, index) => (
          <option value={item.value}>{item.label}</option>
        ))}
      </Select>
    </InputWrapper>
  );
};

export default SelectWrapper;
