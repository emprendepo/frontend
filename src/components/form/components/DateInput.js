import React from "react";
import { Grid, Typography, makeStyles, Select } from "@material-ui/core";
import { getMonthsArray, generateYearsArray } from "../utils/functions";

const inputStyles = makeStyles((theme) => ({
    container: {
        paddingTop: "20px",
        marginBottom: 10,
    },
    label: {
        fontSize: theme.fontSizes[0],
        width: "100%",
    },
    errorColor: {
        color: theme.palette.error.main,
    },
    requiredDot: {
        color: `#0B2D48`,
        fontSize: `1em`,
        marginLeft: 2,
        top: 0,
    },
    error: {
        textAlign: `center`,
        color: theme.palette.error.main,
        lineHeight: `1.6em`,
        fontSize: `1.4em`,
        marginTop: theme.spacing(2),
    },
}));

const time_constants = {
    years: generateYearsArray(120),
    months: getMonthsArray(),
};

export default function DateInput({
    day,
    month,
    year,
    onChange,
    label,
    name,
    error,
    isRequired = false,
}) {
    const classes = inputStyles();
    return (
        <Grid container zeroMinWidth className={classes.container}>
            <Typography
                variant="h6"
                color="primary"
                className={`${error ? classes.errorColor : ``} ${
                    classes.label
                }`}
            >
                {label}
                {isRequired && <sup className={classes.requiredDot}>(*)</sup>}
            </Typography>
            <Grid container item spacing={1}>
                <Grid item xs={3} md={3} lg={3}>
                    <Select
                        native
                        value={day}
                        onChange={({ target: { value, name } }) =>
                            onChange({ name, value })
                        }
                        inputProps={{
                            name: "day",
                        }}
                    >
                        <option value="">Día</option>
                        {new Array(31).fill(undefined).map((_, index) => (
                            <option value={index + 1}>{index + 1}</option>
                        ))}
                    </Select>
                </Grid>
                <Grid item xs={5} md={5} lg={5}>
                    <Select
                        native
                        value={month}
                        onChange={({ target: { value, name } }) =>
                            onChange({ name, value })
                        }
                        inputProps={{
                            name: "month",
                        }}
                    >
                        <option value="">Mes</option>
                        {time_constants.months.map((month, index) => (
                            <option value={index + 1}>{month}</option>
                        ))}
                    </Select>
                </Grid>
                <Grid item xs={4} md={4} lg={4}>
                    <Select
                        native
                        value={year}
                        onChange={({ target: { value, name } }) =>
                            onChange({ name, value })
                        }
                        inputProps={{
                            name: "year",
                        }}
                    >
                        <option value="">Año</option>
                        {time_constants.years.map((year, index) => (
                            <option value={year}>{year.toString()}</option>
                        ))}
                        )}
                    </Select>
                </Grid>
            </Grid>
            {error && error.length > 0 && (
                <Grid container item justify="center">
                    <Grid item>
                        <Typography className={classes.error}>
                            {error}
                        </Typography>
                    </Grid>
                </Grid>
            )}
        </Grid>
    );
}
