import React from "react";
import TextField from "@material-ui/core/TextField";
import InputWrapper from "./InputWrapper";
import { handleValidationValue } from "../../../utils/validations";

const TextInputWrapper = ({
  id,
  label,
  keyName,
  error, //Puede ser boolean o string
  isRequired = false,
  onChange,
  onBlur = null,
  value,
  name,
  type = `text`,
  inputType = `text`,
  min = 0,
  autocompleteData,
  max,
  size = "medium",
  inputProps = {},
  InputProps = {},
  disabled = false,
  placeholder = ``,
  align = `left`,
  fullWidth = true,
  variant,
  xs = 12,
  md = 12,
  ...rest
  //autocompleteValueSelected,
}) => {
  return (
    <InputWrapper
      label={label}
      isRequired={isRequired}
      align={align}
      type={inputType}
      error={error}
      xs={xs}
      md={md}
      disabled={disabled}
      {...rest}
    >
      {type === `label` ? null : (
        <TextField
          placeholder={placeholder}
          id={name}
          value={handleValidationValue(value, keyName)}
          name={name}
          type={type}
          step={"0.1"}
          variant={variant}
          disabled={disabled}
          onChange={({ target: { value, name } }) =>
            onChange({ name, value, id })
          }
          min={type === `number` ? min : null}
          max={max}
          onBlur={onBlur}
          error={error}
          InputLabelProps={{
            shrink: true,
          }}
          fullWidth={fullWidth}
          InputProps={InputProps}
          inputProps={inputProps}
          size={size}
          {...rest}
        />
      )}
    </InputWrapper>
  );
};

export default TextInputWrapper;
