import React, { useEffect, useState } from "react";
import { Upload, Button } from "antd";
import { UploadOutlined } from "@ant-design/icons";
import InputWrapper from "./InputWrapper";
import API from "../../../config/api";
import Loading from "../../Loading";

function FileUpload({
  id,
  itemId,
  label,
  error, //Puede ser boolean o string
  isRequired = false,
  onChange,
  onBlur = null,
  value = [],
  name = "files",
  min = 0,
  autocompleteData,
  max,
  inputType = "file",
  acceptTypes = "image/*",
  fileTypes = [".jgp", ".png"],
  size = "medium",
  inputProps = {},
  InputProps = {},
  disabled = false,
  placeholder = ``,
  align = `left`,
  multiple = false,
  fullWidth = false,
  maxMBFileSize = 2,
  xs = 12,
  md = 12,
  ...rest
}) {
  console.log(value);
  const [list, setList] = useState([]);
  const [isLoading, setIsloading] = useState(true);

  const whatTypeFileAccept = `image/*,.pdf,.mp4`;
  const uploadRequest = async ({ file, onSuccess, onError }) => {
    const formData = new FormData();
    formData.append(`file`, file);
    formData.append(`uid`, file.uid);
    formData.append(`producto_id`, itemId);
    await API.postUploadFile(`files`, formData)
      .then(({ data }) => {
        onSuccess(`ok`);
        onChange({ name, value: [...list, data.data] });
      })
      .catch(() => onError(`error`));
  };
  const handleRemove = async (items) => {
    const arrayList = list.filter(({ id }) => id !== items.id);
    onChange({ name, value: arrayList });
    setList(arrayList);
  };
  useEffect(() => {
    try {
      const list = value?.map((item, key) => ({
        id: item.id,
        uid: `${key}-1`,
        name: item.name,
        url: `https://api.emprendepo.test${
          item?.path
            ? item?.path.replace(`public`, ``)
            : item?.url.replace(`public`, ``)
        }`,
        thumbUrl: `https://api.emprendepo.test${
          item?.path
            ? item?.path.replace(`public`, ``)
            : item?.url.replace(`public`, ``)
        }`,
        status: "done",
      }));
      setList(list);
    } catch (e) {
      setList([]);
    }
    setIsloading(false);
  }, [value]);

  if (isLoading) {
    return <Loading />;
  }

  return (
    <InputWrapper
      label={label}
      isRequired={isRequired}
      align={align}
      type={inputType}
      error={error}
      fullWidth={fullWidth}
      xs={xs}
      md={md}
      disabled={disabled}
      {...rest}
    >
      <Upload
        name={name}
        multiple={false}
        accept={whatTypeFileAccept}
        customRequest={uploadRequest}
        listType="picture"
        defaultFileList={[...list]}
        onRemove={handleRemove}
      >
        <Button icon={<UploadOutlined />}>Upload</Button>
      </Upload>
    </InputWrapper>
  );
}

export default FileUpload;
