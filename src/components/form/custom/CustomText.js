import React from "react";
import { Grid, Typography, makeStyles } from "@material-ui/core";

function CustomText({ props, ChildrenNode = false }) {
    const classes = styles();
    return (
        <Grid
            container
            className={`${
                props.backgroundColor === "primary"
                    ? classes.backgroundPrimary
                    : props.backgroundColor === "dangerLight"
                    ? classes.backgroundDangerLight
                    : ""
            } ${!props.title ? classes.container : ""} ${classes.padding}`}
        >
            {props.title && (
                <Grid container item alignItems={props.title.align}>
                    <Typography
                        variant="h6"
                        className={`${classes.text} ${classes.title}`}
                    >
                        {props.title.value}
                    </Typography>
                </Grid>
            )}
            <Grid item>
                <Typography
                    variant="h6"
                    align={props.justify ? "justify" : "inherit"}
                    className={`${classes.text} ${
                        props.customFontSize ? classes.customFontSize : ""
                    }`}
                    dangerouslySetInnerHTML={{ __html: props.text }}
                />
            </Grid>
            {ChildrenNode && (
                <Grid container item className={classes.childContainer}>
                    {ChildrenNode}
                </Grid>
            )}
        </Grid>
    );
}

const styles = makeStyles((theme) => ({
    text: {
        color: theme.palette.primary.main,
    },
    customFontSize: {
        fontSize: 14,
    },
    title: {
        fontWeight: "bold",
    },
    backgroundPrimary: {
        backgroundColor: "beige",
    },
    padding: {
        padding: theme.spacing(2, 1.5) + `!important`,
    },
    backgroundDangerLight: {
        backgroundColor: "#f8d7db",
    },
    container: {
        padding: theme.spacing(2, 0),
    },
    childContainer: {
        marginTop: theme.spacing(2),
    },
}));

export default CustomText;
