import React from "react";
import Grid from "@material-ui/core/Grid";
import CircularProgress from "@material-ui/core/CircularProgress";

export const Loading = () => (
  <Grid
    xs={12}
    item
    container
    alignItems="center"
    justify="center"
    style={{ height: `100vh` }}
  >
    <CircularProgress size={100} />
  </Grid>
);
export default Loading;
