import React, { useState } from 'react'
import {
  TableCell,
  TableBody,
  TableRow,
  withStyles,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Icon,
} from '@material-ui/core'
import { NavLink } from 'react-router-dom'
import { fecha } from 'utils/format'
import { deleteEntity } from 'utils/apiUtil'
import { Link } from 'react-router-dom'

function Body({ columns, data, options, classes, callBack, userType }) {
  const [open, setOpen] = useState(false)
  const [id, setId] = useState(0)

  const handleItem = id => {
    setOpen(true)
    setId(id)
  }
  const deleteItem = () => {
    if (id !== 0) {
      deleteEntity(options.entity, id).then(() => {
        setOpen(false)
        callBack()
      })
    }
  }

  return (
    <TableBody>
      {data.map(item => {
        const cells = columns.map((colData, index) => {
          if (colData.attribute === ``) {
            return (
              <TableCell className="contrast-white-text" key={index}>
                <a href={`/${options.model}/${item.id}`} title="View">
                  <Icon className={classes.icon}>remove_red_eye</Icon>
                </a>
                &nbsp;
                <a href={`/${options.model}/${item.id}/edit`} title="Edit">
                  <Icon className={classes.icon}>edit</Icon>
                </a>
                &nbsp;
                <Link
                  onClick={handleItem(item.id)}
                  title="Remove"
                  data-toggle="modal"
                  data-target={`#${options.model}-box-modal`}
                >
                  <Icon className={classes.icon}>delete_forever</Icon>
                </Link>
              </TableCell>
            )
          } else if (colData.attribute === `criterios_beneficios.operacion`) {
            return (
              <TableCell className="contrast-white-text" key={index}>
                {item.criterios_beneficios.map((criterio, i) => {
                  return (
                    <div key={i}>
                      -{` `}
                      {`${criterio.data_criterio.nombre} ${criterio.operacion} ${criterio.valor}`}
                      <br />
                    </div>
                  )
                })}
              </TableCell>
            )
          } else if (colData.attribute === `accion`) {
            return (
              <TableCell className="contrast-white-text" key={index}>
                <Button className="benefit-button">Ver ficha</Button>
              </TableCell>
            )
          } else if (colData.attribute === `roles.id`) {
            return (
              <TableCell className="contrast-white-text" key={index}>
                {item.roles
                  .sort((c1, c2) =>
                    c1.institucion.localeCompare(c2.institucion),
                  )
                  .map((role, i) => {
                    return (
                      <div key={i}>
                        {`${role.institucion} - ${role.nombre}`} <br />
                      </div>
                    )
                  })}
              </TableCell>
            )
          } else if (colData.attribute === `url`) {
            return (
              <TableCell className="contrast-white-text" key={index}>
                <NavLink
                  to={`${
                    userType === `asesor`
                      ? `/asesor/fichas/`
                      : `/ciudadano/fichas/`
                  }  ${item[colData.attribute]}`}
                >
                  Ver ficha
                </NavLink>
              </TableCell>
            )
          } else if (colData.attribute === `fecharecepcionultben`) {
            return (
              <TableCell className="contrast-white-text" key={index}>
                {fecha(item[colData.attribute])}
              </TableCell>
            )
          } else {
            return (
              <TableCell className="contrast-white-text" key={index}>
                {item[colData.attribute]}
              </TableCell>
            )
          }
        })
        return <TableRow key={item.id}>{cells}</TableRow>
      })}
      <Dialog
        open={open}
        onClose={() => setOpen(false)}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">¿Estás seguro?</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Msg Borrar regla
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button variant="outlined" onClick={() => setOpen(false)}>
            Cancelar
          </Button>
          <Button onClick={deleteItem} autoFocus>
            Aceptar
          </Button>
        </DialogActions>
      </Dialog>
    </TableBody>
  )
}
const styles = theme => ({
  root: {
    color: theme.palette.text.primary,
  },
  icon: {
    margin: theme.spacing(1),
    fontSize: 32,
    color: theme.palette.text.secondary,
  },
})

export default withStyles(styles)(Body)
