import React from 'react'
import {
  withStyles,
  Table,
  TableCell,
  TableHead,
  TableRow,
} from '@material-ui/core'
import Body from './body.component'

function DataTable({ classes, columns, userType, ...rest }) {
  return (
    <Table className={classes.table}>
      <TableHead>
        <TableRow>
          {columns.map((colData, index) => (
            <TableCell className="contrast-green-text" key={index}>
              {` `}
              {colData.displayName}
              {` `}
            </TableCell>
          ))}
        </TableRow>
      </TableHead>
      <Body userType={userType} columns={columns} {...rest} />
    </Table>
  )
}
const styles = theme => ({
  root: {
    width: `100%`,
    marginTop: theme.spacing(3),
    overflowX: `auto`,
  },
})

export default withStyles(styles)(DataTable)
