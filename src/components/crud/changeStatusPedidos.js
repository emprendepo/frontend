import React, { useState } from "react";
import BuildIcon from "@material-ui/icons/Build";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import API from "../../config/api";
import { Dialog } from "../";

function ChangeStatusPedidos({ items: { estado, id }, callback }) {
  const [open, setOpen] = useState(false);
  const [value, setValue] = useState(estado);

  const handleChange = (event) => {
    setValue(event.target.value);
  };

  const handleAction = async () => {
    await API.updateCustomEntity(id, `pyme/pedidos`, { estado: value });
    setOpen(false);
    callback();
  };

  return (
    <>
      <Dialog
        title={"Estado del Pedido"}
        textActionOk="Actualizar"
        isVisibleActionCancel={false}
        showAction={handleAction}
        maxWidth="sm"
        onClose={() => setOpen(false)}
        openModal={open}
      >
        <FormControl fullWidth variant="filled">
          <InputLabel id="demo-simple-select-filled-label">Estados</InputLabel>
          <Select
            labelId="demo-simple-select-filled-label"
            id="demo-simple-select-filled"
            value={value}
            onChange={handleChange}
          >
            <MenuItem value="Pagado">Pagado</MenuItem>
            <MenuItem value="Enviado">Enviado</MenuItem>
            <MenuItem value="Cancelado">Cancelado</MenuItem>
          </Select>
        </FormControl>
      </Dialog>
      <BuildIcon onClick={() => setOpen(!open)} />
    </>
  );
}

export default ChangeStatusPedidos;
