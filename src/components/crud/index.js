import ChangeStatusPedidos from "./changeStatusPedidos";
import DetalleVenta from "./detalleVenta";
import StatusClientes from "./statusCliente";
import PrintDetalle from "./printDetalle";
export { ChangeStatusPedidos, DetalleVenta, StatusClientes, PrintDetalle };
