import React, { useState } from "react";
import AssignmentIndIcon from "@material-ui/icons/AssignmentInd";
import { Descriptions } from "antd";
import { Dialog } from "../";

function StatusClientes({
  items: {
    boletas: { clientes },
  },
}) {
  const [open, setOpen] = useState(false);
  return (
    <>
      <Dialog
        title={"Detalle Cliente"}
        textActionOk="Actualizar"
        maxWidth="sm"
        onClose={() => setOpen(false)}
        openModal={open}
      >
        <Descriptions bordered>
          <Descriptions.Item label="Rut" span={3}>
            {clientes?.rut}
          </Descriptions.Item>
          <Descriptions.Item label="Nombre" span={3}>
            {clientes?.nombres}
          </Descriptions.Item>
          <Descriptions.Item label="Apellido Paterno" span={3}>
            {clientes?.apellido_paterno}
          </Descriptions.Item>
          <Descriptions.Item label="Apellido Materno" span={3}>
            {clientes?.apellido_materno}
          </Descriptions.Item>
          <Descriptions.Item label="Mail" span={3}>
            {clientes?.email}
          </Descriptions.Item>
          <Descriptions.Item label="Direccion" span={3}>
            {clientes?.direccion}
          </Descriptions.Item>
          <Descriptions.Item label="Telefono" span={3}>
            {clientes?.telefono}
          </Descriptions.Item>
        </Descriptions>
      </Dialog>
      <AssignmentIndIcon onClick={() => setOpen(!open)} />
    </>
  );
}

export default StatusClientes;
