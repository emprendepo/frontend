import React, { useState } from "react";
import ListAltIcon from "@material-ui/icons/ListAlt";
import TableComponent from "../table";
import { Dialog } from "../";

function DetalleVenta({ items: { boletas } }) {
  const [open, setOpen] = useState(false);

  const {
    ventas: { detalle_ventas: detalleVentas = [] },
  } = boletas ?? {};

  const handleImg = (files) => {
    const [file] = files;
    const imageLogo = file
      ? `https://api.emprendepo.test${file?.path.replace(`public`, ``)}`
      : "https://via.placeholder.com/100";
    return <img src={imageLogo} width={100} alt={file?.name} />;
  };

  const columns = [
    {
      title: `Producto`,
      key: `producto`,
      render: ({ productos }) => handleImg(productos?.files) ?? "",
    },
    {
      title: `Descripcion`,
      key: `descripcion`,
      render: ({ productos }) => productos?.nombre ?? "",
    },
    {
      title: `Cantidad`,
      key: `cantidad`,
      render: ({ cantidad }) => cantidad ?? "",
    },
    {
      title: `Valor unidad`,
      key: `total`,
      render: ({ valor }) =>
        new Intl.NumberFormat(`es-CL`, {
          currency: `CLP`,
          style: `currency`,
        }).format(valor) ?? "",
    },
  ];

  return (
    <>
      <Dialog
        title="Detalle del Pedido"
        maxWidth="sm"
        onClose={() => setOpen(false)}
        openModal={open}
      >
        <TableComponent dataSource={detalleVentas} columns={columns} />
      </Dialog>
      <ListAltIcon onClick={() => setOpen(!open)} />
    </>
  );
}

export default DetalleVenta;
