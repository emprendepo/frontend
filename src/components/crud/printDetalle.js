import React from "react";
import LocalPrintshopIcon from "@material-ui/icons/LocalPrintshop";

function PrintDetalle({ id }) {
  return (
    <LocalPrintshopIcon
      onClick={() =>
        window.open("https://api.emprendepo.test/despacho/" + id, `_blank`)
      }
    />
  );
}

export default PrintDetalle;
