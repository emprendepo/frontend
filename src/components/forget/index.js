import React, { useState, useEffect } from "react";
import { Grid } from "@material-ui/core";
import Form from "../../components/form/";
import Dialog from "../../components/Dialog";
import { formForget } from "./data";
import { handleChangeForm } from "../../utils";
import API from "../../config/api";

function Forget() {
  const [form, setForm] = useState(formForget);
  const [modal, setModal] = useState(false);
  const [loading, setLoading] = useState(false);
  const [isValid, setIsValid] = useState(false);
  const [generalFormError, setGeneralFormError] = useState(false);

  const onChangeForm = ({ name, value }) => {
    let nextForm = handleChangeForm(form, name, value);
    value = nextForm[name].value;
    setForm(nextForm);
  };

  const Auth = async () => {
    setGeneralFormError(false);
    const {
      email: { value: email },
    } = form;

    try {
      setLoading(true);
      await API.createCustomEntity(`public/user/validate/mail`, {
        email,
      });
      setModal(false);
      alert(`Se envio un email con las instrucciones a seguir.`);
    } catch (error) {
      setGeneralFormError(error);
      setLoading(false);
    }
  };

  useEffect(() => {
    let nextIsValid = true;
    Object.keys(form).forEach((name) => {
      const { value, error, inputRequired, inputType } = form[name];
      if (
        (inputRequired &&
          (!value || (inputType === "Select" && value === -1))) ||
        (error && error.length > 0)
      ) {
        nextIsValid = false;
      }
    });
    if (nextIsValid) {
      setGeneralFormError(false);
    }
    setIsValid(nextIsValid);
  }, [form]);

  return (
    <>
      <Dialog
        maxWidth="sm"
        title="¿Olvidaste tu contraseña?"
        openModal={modal}
        onClose={() => setModal(false)}
      >
        <Form
          formSubName={`Ingresa un email registrado para recibir el link para recuperar tu contraseña.`}
          form={form}
          onChangeForm={onChangeForm}
          submitForm={Auth}
          isValid={isValid}
          loading={loading}
          hideSubmitButton={false}
          formError={generalFormError}
        />
      </Dialog>
      <Grid item>
        <p onClick={() => setModal(true)}>¿Olvidaste tu contraseña?</p>
      </Grid>
    </>
  );
}

export default Forget;
