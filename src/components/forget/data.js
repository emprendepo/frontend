import { EmailValid } from "../../utils/validations";

export const formForget = {
  email: {
    value: ``,
    error: false,
    label: "Correo electrónico",
    placeholder: "",
    inputType: "TextInput",
    inputRequired: true,
    rules: { ...EmailValid },
  },
};
