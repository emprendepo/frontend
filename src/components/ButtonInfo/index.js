import React from "react";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import CircularProgress from "@material-ui/core/CircularProgress";
import { makeStyles } from "@material-ui/core/styles";

const styleButton = (palette, breakpoints) => {
  return {
    borderRadius: `3px`,
    color: palette.common.white,
    fontStyle: `normal`,
    fontWeight: 500,
    fontSize: ` 18px`,
    lineHeight: `45px`,
    [breakpoints.down(`sm`)]: {
      fontSize: `16px`,
      lineHeight: `36px`,
      height: 51,
    },
  };
};

const useStyles = makeStyles(({ palette, breakpoints }) => ({
  root: {
    marginTop: 10,
  },
  primary: {
    backgroundColor: palette.primary.main,
    ...styleButton(palette, breakpoints),
    "&:hover": {
      backgroundColor: palette.common.white,
      border: `2px solid ${palette.primary.main}`,
      boxSizing: ` border-box`,
      color: palette.primary.main,
    },
  },
  secondary: {
    backgroundColor: palette.common.white,
    border: `2px solid ${palette.secondary.main}`,
    ...styleButton(palette, breakpoints),
    color: palette.secondary.main,
    "&:hover": {
      backgroundColor: palette.secondary.main,
      border: `2px solid ${palette.secondary.main}`,
      boxSizing: ` border-box`,
      color: palette.common.white,
    },
  },
  terniary: {
    backgroundColor: palette.common.white,
    border: `2px solid ${palette.primary.dark}`,
    ...styleButton(palette, breakpoints),
    color: `${palette.primary.dark}`,
    "&:hover": {
      backgroundColor: palette.primary.dark,
      border: `2px solid ${palette.primary.dark}`,
      boxSizing: ` border-box`,
      color: palette.common.white,
    },
  },
  text: {
    ...styleButton(palette, breakpoints),
    color: palette.other.letra,
    lineHeight: `19px`,
    border: `none`,
    backgroundColor: `white`,
    maxHeight: 20,
    minHeight: 20,
    fontSize: `16px`,
    [breakpoints.down(`sm`)]: {
      fontSize: `14px`,
    },
  },
}));

function ButtonInfo({
  align = `center`,
  type = `primary`,
  handleOnClick = null,
  full = true,
  isLoading = false,
  text = `Registrarse`,
  style = {},
  color = `default`,
  isDisabled = false,
}) {
  const classes = useStyles();

  return (
    <Grid
      container
      direction="row"
      className={classes.root}
      justify="center"
      alignItems={align}
    >
      {isLoading ? (
        <CircularProgress />
      ) : (
        <Button
          disabled={isDisabled}
          color={color}
          style={{ ...style }}
          fullWidth={full}
          className={classes[type]}
          onClick={() => handleOnClick()}
        >
          {text}
        </Button>
      )}
    </Grid>
  );
}
export default ButtonInfo;
