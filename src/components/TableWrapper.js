import React from "react";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import { Button, makeStyles } from "@material-ui/core";

const styles = makeStyles({
    noBorder: {
        border: "none",
    },
});

export default function TableWrapper({
    rows,
    headers,
    headerRut = false,
    size = "medium",
    titleClass = false,
    containerTitleClass = "",
    setModalMotivos = false,
    linkButtonClass = false,
    headerAlign = "left",
    rowAlign = "left",
    noBorder = false,
}) {
    const classes = styles();
    const formatMotivos = (motivos) => {
        let body = "";
        motivos.map((motivo) => {
            body += `- ${motivo}<br/>`;
            return false;
        });
        return body;
    };
    const openModalAndSendData = (data) => {
        setModalMotivos({
            open: true,
            body: data,
            title:
                "Estos son los motivos por los que tu subsidio no fue otorgado:",
        });
    };
    if (!rows || rows.length === 0 || !headers || headers.length === 0) {
        return false;
    }
    return (
        <TableContainer component={Paper}>
            <Table size={size} className={classes.root}>
                <TableHead>
                    <TableRow className={containerTitleClass}>
                        {headerRut && (
                            <TableCell
                                className={`${
                                    noBorder ? classes.noBorder : ""
                                } ${titleClass}`}
                                colSpan={headers.length}
                                align="left"
                            >
                                {headerRut}
                            </TableCell>
                        )}
                    </TableRow>
                    <TableRow>
                        {headers.map((header, index) => (
                            <TableCell
                                className={noBorder ? classes.noBorder : ""}
                                key={index}
                                align={headerAlign}
                            >
                                {header}
                            </TableCell>
                        ))}
                    </TableRow>
                </TableHead>
                <TableBody>
                    {rows.map((row, index) => (
                        <TableRow key={index}>
                            {headers.map((header, index2) => {
                                const rowName = row[header.replace(/ /g, "")];
                                if (
                                    setModalMotivos &&
                                    header === "Motivo NO otorgamiento"
                                ) {
                                    let motivos = rowName.split("|");
                                    if (motivos.length > 1) {
                                        motivos = formatMotivos(motivos);
                                        return (
                                            <TableCell
                                                key={index2}
                                                align="left"
                                                className={
                                                    noBorder
                                                        ? classes.noBorder
                                                        : ""
                                                }
                                            >
                                                <Button
                                                    className={`${linkButtonClass}`}
                                                    variant="outlined"
                                                    onClick={() =>
                                                        openModalAndSendData(
                                                            motivos
                                                        )
                                                    }
                                                >
                                                    Ver motivos
                                                </Button>
                                            </TableCell>
                                        );
                                    }
                                }
                                return (
                                    <TableCell
                                        className={
                                            noBorder ? classes.noBorder : ""
                                        }
                                        key={index2}
                                        align={rowAlign}
                                    >
                                        {rowName}
                                    </TableCell>
                                );
                            })}
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
    );
}
