import React, { useContext, useState, useEffect } from "react";
import Loading from "../components/Loading";
import API from "../config/api";
import { validateUrl } from "../utils";

const AuthContext = React.createContext();
const initialState = () => {
  const id = localStorage.getItem(`token`) || null;
  return id;
};

function AuthProvider({ children }) {
  const [user, setUser] = useState(null);
  const [id, setID] = useState(initialState());
  const [loading, setLoading] = useState(false);

  const getMe = async () => {
    try {
      setLoading(true);
      const nextUser = await API.me();
      setUser(nextUser);
      setLoading(false);
    } catch (error) {
      setID(null);
      localStorage.removeItem(`token`);
      return (window.location.href = "/");
    }
  };

  useEffect(() => {
    id && getMe();
  }, [id]);

  function logIn({ token, roles }) {
    localStorage.setItem(`token`, `Bearer ${token}`);
    setID(token);
    return (window.location.href = validateUrl(roles));
  }
  function logOut() {
    localStorage.removeItem(`token`);
    setUser(null);
    return (window.location.href = "/");
  }

  return (
    <AuthContext.Provider
      value={{
        user,
        logIn,
        logOut,
        setUser,
      }}
    >
      {loading ? <Loading /> : children}
    </AuthContext.Provider>
  );
}

function useAuth() {
  const context = useContext(AuthContext);
  if (!context) {
    throw new Error(`useAuth must be used within a AuthProvider`);
  }
  return context;
}

export { AuthProvider, useAuth };
