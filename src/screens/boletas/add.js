import React, { useState, useEffect } from "react";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core";
import { withRouter } from "react-router-dom";
import { Table, Typography as antTypography, Descriptions } from "antd";
import AddCircleOutlineOutlinedIcon from "@material-ui/icons/AddCircleOutlineOutlined";
import NumberFormat from "react-number-format";
import CrudModal from "../crud/crudModal";
import API from "../../config/api";
import Content from "../content";
import { Loading } from "../../components";

const { Text } = antTypography;
const homeStyles = makeStyles((theme) => ({
  button: {
    textDecoration: `none`,
    marginBottom: theme.spacing(2),
    [theme.breakpoints.down(`sm`)]: {},
  },
  descripcion: {
    padding: theme.spacing(2),
  },
}));

function AddBoletas({ history }) {
  const classes = homeStyles();
  const [loading, setLoading] = useState(false);
  const [items, setItems] = useState([]);
  const [data, setData] = useState([]);
  const [form, setForm] = useState(null);
  const [productos, setProductos] = useState(null);
  const [MetodoDePagos, setMetodo] = useState(null);
  const [clientes, setClientes] = useState(null);
  const [detalle, setDetalle] = useState({
    clienteName: ``,
    trabajadorName: ``,
    metodoName: ``,
    fechaVenta: ``,
  });
  const [modal, setModal] = useState({
    show: false,
    data: {},
    type: ``,
  });

  const columns = [
    {
      title: "Index",
      render: (text, record, index) => {
        return index + 1;
      },
    },
    {
      title: "Producto",
      render: (item) => {
        return item && item.name;
      },
    },
    {
      title: "Cantidad",
      render: (item) => {
        return item && item.cantidad;
      },
    },
    {
      title: "Valor",
      render: (item) => {
        return item && item.valor;
      },
    },
    {
      title: "Sub Total",
      render: (item) => {
        return item && item.valor * item.cantidad;
      },
    },
  ];

  const handleAddItem = () => {
    setForm(data.form);
    setModal({
      ...modal,
      show: true,
      data: { libro: ``, valor: ``, cantidad: `` },
      method: CreateItem,
      type: `add`,
    });
  };

  const handleAddDetalle = () => {
    setForm(data.formDetalle);
    setModal({
      ...modal,
      show: true,
      data: { cliente: ``, trabajador: ``, metodoDePago: ``, fechaFactura: `` },
      method: CreateDetalle,
      type: `add`,
    });
  };

  const CreateItem = ({ productos: producto, cantidad, valor, ...props }) => {
    const newLibro = productos.filter((item) => item.value === producto);
    var newData = [
      ...items,
      { name: newLibro[0].label, cantidad, valor, idProducto: producto },
    ];
    setItems(newData);
    setModal({ ...modal, show: false });
  };

  const CreateDetalle = (
    { cliente = ``, trabajador = ``, metodoDePago = ``, fechaBoleta = `` },
    ...props
  ) => {
    const newCliente = clientes.filter((item) => item.value === cliente);
    const newMetodoPago = MetodoDePagos.filter(
      (item) => item.value === metodoDePago
    );
    var newDetalle = {
      clienteName: newCliente[0].label,
      metodoName: newMetodoPago[0].label,
      fechaBoleta,
      clienteID: newCliente[0].value,
      metodoPagoId: newMetodoPago[0].value,
    };

    setDetalle(newDetalle);
    setModal({ ...modal, show: false });
  };

  const hadleAddBoleta = async () => {
    const newBoleta = {
      items,
      detalle,
    };
    const boleta = await API.createCustomEntity(`boleta/form`, newBoleta);
    boleta.id && history.push(`/crud/boletas`);
  };

  const getData = async () => {
    setLoading(true);
    const data = await API.getCustomEntity(`boleta/form`);
    setData(data);
    setProductos(data.productos);
    setClientes(data.clientes);
    setMetodo(data.metodosDePago);
    setLoading(false);
  };

  useEffect(() => {
    getData();
    // eslint-disable-next-line
  }, []);

  if (loading) {
    return <Loading />;
  }

  return (
    <Content>
      {modal.show && (
        <CrudModal
          modal={modal}
          errors={null}
          onClose={() => setModal({ ...modal, show: false })}
          entity={`Boletas`}
          form={form}
          create={modal.method}
        />
      )}
      <Grid item container xs={12} alignItems="center">
        <Grid item xs={12} md={8}>
          <Typography variant="h2" className={classes.modalTitle}>
            Agregar Boleta
          </Typography>
        </Grid>
        <Grid item xs={12} md={4} container justify="flex-end">
          <Grid container>
            <Grid item md={6}>
              <Button
                variant="contained"
                className={classes.button}
                onClick={handleAddItem}
              >
                {`Agregar item boleta`}
                <AddCircleOutlineOutlinedIcon />
              </Button>
            </Grid>
            <Grid item md={6}>
              <Button
                variant="contained"
                className={classes.button}
                onClick={handleAddDetalle}
              >
                {`Detalle boleta`}
                <AddCircleOutlineOutlinedIcon />
              </Button>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
      <Grid container item xs={12} className={classes.descripcion}>
        <Descriptions title="Detalle Boleta" bordered>
          <Descriptions.Item label="Cliente">
            {detalle.clienteName}
          </Descriptions.Item>
          <Descriptions.Item label="Metodo de Pago">
            {detalle.metodoName}
          </Descriptions.Item>
          <Descriptions.Item label="Fecha Boleta">
            {detalle.fechaBoleta}
          </Descriptions.Item>
        </Descriptions>
        ,
      </Grid>
      <Grid item xs={12}>
        <Table
          loading={false}
          dataSource={items}
          columns={columns}
          summary={(pageData) => {
            let totalBorrow = 0;
            let totalRepayment = 0;
            let totalCantidad = 0;

            pageData.forEach(({ valor, cantidad }) => {
              totalRepayment += valor * cantidad;
              totalBorrow += parseInt(valor);
              totalCantidad += parseInt(cantidad);
            });

            return (
              <>
                <Table.Summary.Row>
                  <Table.Summary.Cell>Total</Table.Summary.Cell>
                  <Table.Summary.Cell></Table.Summary.Cell>
                  <Table.Summary.Cell>{totalCantidad}</Table.Summary.Cell>
                  <Table.Summary.Cell>
                    <Text type="danger">
                      <NumberFormat
                        decimalSeparator={"."}
                        value={totalBorrow}
                        displayType={"text"}
                        thousandSeparator={true}
                        prefix={"$"}
                      />
                    </Text>
                  </Table.Summary.Cell>
                  <Table.Summary.Cell>
                    <Text>
                      <NumberFormat
                        decimalSeparator={"."}
                        value={totalRepayment * totalCantidad}
                        displayType={"text"}
                        thousandSeparator={true}
                        prefix={"$"}
                      />
                    </Text>
                  </Table.Summary.Cell>
                </Table.Summary.Row>
              </>
            );
          }}
        />
      </Grid>
      <Grid
        container
        alignItems="center"
        justify="center"
        className={classes.descripcion}
      >
        <Button
          disabled={items.length < 1}
          variant="contained"
          className={classes.button}
          onClick={hadleAddBoleta}
        >
          {`Agregar Boleta`}
          <AddCircleOutlineOutlinedIcon />
        </Button>
      </Grid>
    </Content>
  );
}

export default withRouter(AddBoletas);
