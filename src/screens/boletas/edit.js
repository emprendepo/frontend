import React, { useState, useEffect } from "react";
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core";
import { withRouter } from "react-router-dom";
import { Table, Typography as antTypography, Descriptions } from "antd";
import API from "../../config/api";
import Content from "../content";
import { Loading } from "../../components";
import moment from "moment";
const { Text } = antTypography;
const homeStyles = makeStyles((theme) => ({
  button: {
    textDecoration: `none`,
    marginBottom: theme.spacing(2),
    [theme.breakpoints.down(`sm`)]: {},
  },
  descripcion: {
    padding: theme.spacing(2),
  },
}));

function EditBoleta({
  history,
  match: {
    params: { id },
  },
}) {
  const classes = homeStyles();
  const [loading, setLoading] = useState(false);
  const [items, setItems] = useState([]);
  const [detalle, setDetalle] = useState({
    clienteName: ``,
    trabajadorName: ``,
    metodoName: ``,
    fechaVenta: ``,
  });

  const columns = [
    {
      title: "Index",
      render: (text, record, index) => {
        return index + 1;
      },
    },
    {
      title: "Producto",
      render: (item) => {
        return item?.productos?.nombre ?? "";
      },
    },
    {
      title: "Cantidad",
      render: (item) => {
        return item.cantidad ?? 0;
      },
    },
    {
      title: "Valor Neto",
      render: (item) => {
        const valor = item?.productos?.valor / 1.19;
        return new Intl.NumberFormat(`es-CL`, {
          currency: `CLP`,
          style: `currency`,
        }).format(valor);
      },
    },
    {
      title: "Valor Total",
      render: (item) => {
        return (
          new Intl.NumberFormat(`es-CL`, {
            currency: `CLP`,
            style: `currency`,
          }).format(item?.productos?.valor) ?? 0
        );
      },
    },
  ];

  const getData = async (id) => {
    setLoading(true);
    const data = await API.getCustomEntity(`boleta/detalle/${id}`);
    setDetalle(data);
    setItems(data?.ventas?.detalle_ventas ?? []);
    setLoading(false);
  };

  useEffect(() => {
    id && getData(id);
    // eslint-disable-next-line
  }, [id]);

  if (loading) {
    return <Loading />;
  }

  return (
    <Content>
      <Grid container item xs={12} className={classes.descripcion}>
        <Descriptions title={`Boleta folio : ${detalle.folio}`} bordered>
          <Descriptions.Item label="Cliente">
            {detalle?.clientes?.nombres}
          </Descriptions.Item>
          <Descriptions.Item label="Metodo de Pago">
            {detalle?.metododepago?.nombre}
          </Descriptions.Item>
          <Descriptions.Item label="Fecha Boleta">
            {moment(detalle?.created_at).format("DD-MM-YYYY")}
          </Descriptions.Item>
        </Descriptions>
        ,
      </Grid>
      <Grid item xs={12}>
        <Table
          loading={false}
          dataSource={items}
          columns={columns}
          summary={(pageData) => {
            let totalBorrow = 0;
            let totalCantidad = 0;
            let totalIva = 0;

            pageData.forEach(({ valor, cantidad }) => {
              totalBorrow += parseInt(valor);
              totalIva += parseInt(valor / 1.19);
              totalCantidad += parseInt(cantidad);
            });

            return (
              <>
                <Table.Summary.Row>
                  <Table.Summary.Cell>Total</Table.Summary.Cell>
                  <Table.Summary.Cell></Table.Summary.Cell>
                  <Table.Summary.Cell>{totalCantidad}</Table.Summary.Cell>
                  <Table.Summary.Cell>
                    <Text type="danger">
                      {new Intl.NumberFormat(`es-CL`, {
                        currency: `CLP`,
                        style: `currency`,
                      }).format(totalIva)}
                    </Text>
                  </Table.Summary.Cell>

                  <Table.Summary.Cell>
                    <Text type="danger">
                      {new Intl.NumberFormat(`es-CL`, {
                        currency: `CLP`,
                        style: `currency`,
                      }).format(totalBorrow)}
                    </Text>
                  </Table.Summary.Cell>
                </Table.Summary.Row>
              </>
            );
          }}
        />
      </Grid>
    </Content>
  );
}

export default withRouter(EditBoleta);
