import React from "react";
import Paper from "@material-ui/core/Paper";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import { Ventas, Clientes, Productos } from "./pages";
import { DatePicker } from "antd";
const { RangePicker } = DatePicker;

const useStyles = makeStyles((theme) => ({
  root: {
    marginTop: 50,
  },
}));

export default function Informes() {
  const classes = useStyles();
  const [date, setDate] = React.useState(null);
  const [value, setValue] = React.useState(
    localStorage.getItem(`tabsInformes`) ?? 0
  );
  const handleChange = (event, newValue) => {
    localStorage.setItem(`tabs`, newValue);
    setValue(newValue);
  };

  const handleComponent = (tab) =>
    ({
      0: <Ventas date={date} />,
      1: <Clientes date={date} />,
      2: <Productos date={date} />,
    }[tab] || 0);

  return (
    <Grid className={classes.root}>
      <Paper>
        <RangePicker onChange={(value, dateString) => setDate(dateString)} />
        <Tabs
          value={value}
          indicatorColor="primary"
          textColor="primary"
          onChange={handleChange}
          aria-label="disabled tabs example"
        >
          <Tab label="Ventas" />
          <Tab label="Clientes" />
          <Tab label="Productos" />
        </Tabs>

        {handleComponent(value)}
      </Paper>
    </Grid>
  );
}
