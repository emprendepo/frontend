import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Typography } from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import { Table } from "antd";
import API from "config/api";
import { ButtonInfo, Loading } from "components";

const useStyles = makeStyles(({ breakpoints }) => ({
  paperContainer: {
    marginTop: 50,
    [breakpoints.down(`sm`)]: {
      padding: `20px 8px 10px 8px !important`,
    },
  },
}));
function Ventas({ date }) {
  const classes = useStyles();
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(false);
  const [data, setData] = useState([]);

  const handleTableClientes = (dataSource) => {
    return {
      dataSource,
      columns: [
        {
          title: "id",
          dataIndex: "id",
          key: "id",
        },
        {
          title: "Boleta Folio",
          dataIndex: "boleta_folio",
          key: "boleta_folio",
        },
        {
          title: "Precio Neto",
          dataIndex: "precio_neto",
          key: "precio_neto",
        },
        {
          title: "Precio con iva",
          dataIndex: "precio_con_iva",
          key: "precio_con_iva",
        },
        {
          title: "Costo Iva",
          dataIndex: "costo_iva",
          key: "costo_iva",
        },
        {
          title: "Medio de pago",
          dataIndex: "medio_de_pago",
          key: "medio_de_pago",
        },
        {
          title: "Fecha Creacion",
          dataIndex: "fecha",
          key: "fecha",
        },
      ],
    };
  };

  const handleExport = async () => {
    try {
      setIsLoading(true);
      setError("");
      const [startDate, endDate] = date;
      const body = { informe: "Ventas", startDate, endDate, export: true };
      await API.download(`pyme/informes`, `informe-ventas`, `xlsx`, {
        ...body,
      });
      setIsLoading(false);
    } catch (e) {
      alert("Ocurrio un Error al exportar");
      setIsLoading(false);
    }
  };

  useEffect(() => {
    if (date) {
      const getData = async () => {
        try {
          setIsLoading(true);
          setError("");
          const [startDate, endDate] = date;
          const body = { informe: "Ventas", startDate, endDate };
          const data = await API.createCustomEntity(`pyme/informes`, {
            ...body,
          });
          setData(data);
          setIsLoading(false);
        } catch (e) {
          setError("Ocurrio un Error");
          setIsLoading(false);
        }
      };
      getData();
    }
  }, [date]);

  if (isLoading) {
    return <Loading />;
  }

  if (error) {
    return (
      <Grid
        container
        justify="center"
        alignContent="center"
        className={classes.paperContainer}
      >
        <Typography variant="h4" align="center">
          {error}
        </Typography>
      </Grid>
    );
  }

  return (
    <Paper className={classes.paperContainer}>
      <Grid
        container
        justify="center"
        alignContent="center"
        className={classes.root}
      >
        <Grid item container alignContent="center" md={10}>
          <Typography variant="h6" align="left">
            Lista de ventas
          </Typography>
        </Grid>
        <Grid item container justify="flex-end" alignContent="center" md={1}>
          {data?.length && (
            <ButtonInfo
              full="false"
              text="exportar"
              handleOnClick={handleExport}
            />
          )}
        </Grid>
        <Grid item md={12} style={{ margin: 10 }}>
          <Table {...handleTableClientes(data)} />
        </Grid>
      </Grid>
    </Paper>
  );
}

export default Ventas;
