import React from "react";
import commonStyles from "../utils/commonInlineStyles";
import { Grid, makeStyles } from "@material-ui/core";

const homeStyles = makeStyles((theme) => ({
  ...commonStyles,
  wrapper: {
    ...commonStyles.wrapper,
    [theme.breakpoints.down("md")]: {
      justifyContent: "center",
    },
  },
}));

export default function Home() {
  const classes = homeStyles();
  return (
    <Grid container className={classes.wrapper}>
      aaaa
    </Grid>
  );
}
