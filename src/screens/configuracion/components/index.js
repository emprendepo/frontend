import Principal from "./principal";
import Direccion from "./direccion";
import Logos from "./logos";
import Suscripcion from "./suscripcion";
import InfoUser from "./infoUser";

export { Principal, Direccion, Logos, Suscripcion, InfoUser };
