import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import { useAuth } from "../../../context";
import { Button, Upload } from "antd";
import { UploadOutlined } from "@ant-design/icons";
import API from "../../../config/api";

const useStyles = makeStyles((theme) => ({
  root: {
    marginTop: 50,
    padding: 20,
  },
  item: {
    padding: 20,
  },
}));

export default function Logos() {
  const { user, setUser } = useAuth();
  const classes = useStyles();

  const handleImg = (name) => {
    const {
      empresa: { logo, fondo },
    } = user;

    const imageLogo = logo
      ? `https://api.emprendepo.test${logo.replace(`public`, ``)}`
      : "https://via.placeholder.com/100";
    const imageFondo = fondo
      ? `https://api.emprendepo.test${fondo.replace(`public`, ``)}`
      : "https://via.placeholder.com/500";

    if (name === "Logo") {
      return imageLogo;
    }

    if (name === "Fondo") {
      return imageFondo;
    }
  };

  const uploadRequest = async ({ file, onSuccess, onError, filename }) => {
    const formData = new FormData();
    formData.append(`file`, file);
    formData.append(`name`, filename);
    formData.append(`uid`, file.uid);
    formData.append(`empresa`, user?.empresa?.id);
    await API.postUploadFile(`pyme/logos`, formData)
      .then(({ data }) => {
        onSuccess(`ok`);
        setUser({ ...user, empresa: { ...data } });
      })
      .catch(() => onError(`error`));
  };

  return (
    <Grid container className={classes.root}>
      <Grid item md={6}>
        <Upload name="fileLogo" customRequest={uploadRequest}>
          <Button icon={<UploadOutlined />}>Actualizar Logo</Button>
        </Upload>
      </Grid>
      <Grid item md={6} className={classes.item}>
        <img width={100} src={handleImg("Logo")} alt={"img"} />
      </Grid>
      <Grid item md={6}>
        <Upload name="fileBackground" customRequest={uploadRequest}>
          <Button icon={<UploadOutlined />}>Actualizar Fondo</Button>
        </Upload>
      </Grid>
      <Grid item md={6} className={classes.item}>
        <img src={handleImg("Fondo")} alt={"img"} />
      </Grid>
    </Grid>
  );
}
