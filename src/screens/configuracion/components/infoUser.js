import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import { useAuth } from "../../../context";
import { formInfoUser } from "../forms";
import Form from "../../../components/form/";
import { handleChangeForm } from "../../../utils";
import API from "../../../config/api";

const useStyles = makeStyles((theme) => ({
  root: {
    marginTop: 50,
  },
}));

export default function InfoUser() {
  const { user, setUser } = useAuth();
  const classes = useStyles();
  const [form, setForm] = useState(formInfoUser(user));
  const [loading, setLoading] = useState(false);
  const [isValid, setIsValid] = useState(false);
  const [generalFormError, setGeneralFormError] = useState(false);

  const onChangeForm = ({ name, value }) => {
    let nextForm = handleChangeForm(form, name, value);
    value = nextForm[name].value;
    setForm(nextForm);
  };

  const Auth = async () => {
    const finallyDataRef = {};
    Object.keys(form).map((item) => (finallyDataRef[item] = form[item].value));
    const data = await API.createCustomEntity(`User/update`, finallyDataRef);
    alert(`Datos Actualizados.`);
    setUser({ ...user, empresa: { ...data } });
  };

  useEffect(() => {
    setLoading(false);
    let nextIsValid = true;
    Object.keys(form).forEach((name) => {
      const { value, error, inputRequired, inputType } = form[name];
      if (
        (inputRequired &&
          (!value || (inputType === "Select" && value === -1))) ||
        (error && error.length > 0)
      ) {
        nextIsValid = false;
      }
    });
    if (nextIsValid) {
      setGeneralFormError(false);
    }
    setIsValid(nextIsValid);
  }, [form]);

  return (
    <Grid
      container
      justify="center"
      alignContent="center"
      className={classes.root}
    >
      <Grid item container md={6}>
        <Form
          formName="Datos Personales"
          form={form}
          textSubmitForm="Guardar"
          onChangeForm={onChangeForm}
          submitForm={Auth}
          isValid={isValid}
          loading={loading}
          hideSubmitButton={false}
          formError={generalFormError}
        />
      </Grid>
    </Grid>
  );
}
