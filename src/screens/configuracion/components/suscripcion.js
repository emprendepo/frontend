import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Loading from "../../../components/Loading";
import { useAuth } from "../../../context";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import moment from "moment";
import API from "../../../config/api";
import { ButtonInfo as Button } from "../../../components";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";

const useStyles = makeStyles((theme) => ({
  root: {
    marginTop: 50,
  },
}));

export default function Direccion() {
  const { user, setUser } = useAuth();
  const classes = useStyles();
  const [loading, setLoading] = useState(false);
  const [type, setType] = useState(user?.suscripciones?.type ?? 0);

  const lista = {
    0: [
      "Agregar hsta 5 productos.",
      "Ver listado de pedidos",
      "Agregar Categorias",
      "Ver Boletas",
      "Cambiar Configuracion.",
    ],
    1: [
      "Todo lo del plan basico",
      "Agregar productos ilimitados",
      "Ver/agregar clientes",
      "Dashboard",
    ],
    2: ["Todo lo del plan premium", "Ver Informes"],
  };

  const handleDeleteInscripcion = async () => {
    try {
      setLoading(true);
      await API.deleteCustomEntity("", `pyme/suscripcion`);
      setUser({ ...user, suscripciones: null });
      setLoading(false);
    } catch (e) {
      setLoading(false);
      alert(e);
    }
  };

  const handleAddIncripcion = async () => {
    try {
      setLoading(true);
      const {
        data: { urlWebpay, token },
      } = await API.createCustomEntity(`pyme/suscripcion`, { type });
      debugger;
      return (window.location.href = `${urlWebpay}?TBK_TOKEN=${token}`);
    } catch (e) {
      setLoading(false);
      alert(e);
    }
  };

  const handleInscrito = () => {
    const { suscripciones } = user;
    return (
      <Grid
        container
        classes={classes.root}
        direction="row"
        justify="center"
        alignContent="center"
        spacing={2}
      >
        <Grid item md={5}>
          <TableContainer component={Paper}>
            <Table aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell>Detalle</TableCell>
                  <TableCell></TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                <TableRow key={suscripciones?.id}>
                  <TableCell component="th" scope="row">
                    Codigo de autorización
                  </TableCell>
                  <TableCell component="th" scope="row">
                    {suscripciones.authorization_code}
                  </TableCell>
                </TableRow>
                <TableRow key={suscripciones?.id}>
                  <TableCell component="th" scope="row">
                    Tipo de tarjeta
                  </TableCell>
                  <TableCell component="th" scope="row">
                    {suscripciones.card_type}
                  </TableCell>
                </TableRow>
                <TableRow key={suscripciones?.id}>
                  <TableCell component="th" scope="row">
                    Numero de la tarjeta
                  </TableCell>
                  <TableCell component="th" scope="row">
                    {suscripciones.card_number}
                  </TableCell>
                </TableRow>
                <TableRow key={suscripciones?.id}>
                  <TableCell component="th" scope="row">
                    Fecha Inscripción
                  </TableCell>
                  <TableCell component="th" scope="row">
                    {moment(suscripciones.created_at).format("DD-MM-Y")}
                  </TableCell>
                </TableRow>
                <TableRow key={suscripciones?.id}>
                  <TableCell component="th" scope="row">
                    Plan
                  </TableCell>
                  <TableCell component="th" scope="row">
                    {suscripciones?.type === "1" ? "Plan Premium" : "Plan Pro"}
                  </TableCell>
                </TableRow>
                <TableRow key={suscripciones?.id}>
                  <TableCell component="th" scope="row">
                    Valor
                  </TableCell>
                  <TableCell component="th" scope="row">
                    {suscripciones?.type === "1" ? "$20.990" : "$26.990"}
                  </TableCell>
                </TableRow>
              </TableBody>
            </Table>
          </TableContainer>
          <Button
            fullWidth
            text="Eliminar Suscripción"
            size="large"
            color="secondary"
            variant="outlined"
            style={{ marginTop: 20 }}
            handleOnClick={handleDeleteInscripcion}
          />
        </Grid>
      </Grid>
    );
  };

  const handleNoInscrito = () => {
    return (
      <Grid
        container
        classes={classes.root}
        direction="row"
        justify="center"
        alignContent="center"
        spacing={2}
      >
        <Grid item md={5}>
          <TableContainer component={Paper}>
            <Table aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell>Suscribete</TableCell>
                  <TableCell></TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                <TableRow key="inscripcion">
                  <TableCell component="th" scope="row">
                    Disfruta de mas opciones para gestionar tu negocio,tienes
                    distintos planes a elegir, selecciona uno:
                  </TableCell>
                </TableRow>
              </TableBody>
            </Table>
          </TableContainer>
          <br />
          <FormControl
            fullWidth
            variant="filled"
            className={classes.formControl}
          >
            <InputLabel id="demo-simple-select-label">
              Planes Ssucripción
            </InputLabel>
            <Select
              fullWidth
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={type}
              onChange={(event) => setType(event.target.value)}
            >
              <MenuItem value={0}>Plan Basico ($0)</MenuItem>
              <MenuItem value={1}>Plan Premium ($20.990)</MenuItem>
              <MenuItem value={2}>Plan Pro ($26.990)</MenuItem>
            </Select>
          </FormControl>
          <br />
          <TableContainer component={Paper}>
            <Table aria-label="simple table">
              <TableBody>
                <TableRow key="inscripcion">
                  <TableCell component="th" scope="row">
                    <ul>
                      {lista[type].map((text) => {
                        return <li>{text}</li>;
                      })}
                    </ul>
                  </TableCell>
                </TableRow>
              </TableBody>
            </Table>
          </TableContainer>

          {type !== 0 && (
            <Button
              text="Suscribete"
              fullWidth
              size="large"
              color="secondary"
              variant="outlined"
              style={{ marginTop: 20 }}
              handleOnClick={handleAddIncripcion}
            />
          )}
        </Grid>
      </Grid>
    );
  };

  if (loading) {
    return <Loading />;
  }
  return (
    <div>{user?.suscripciones ? handleInscrito() : handleNoInscrito()}</div>
  );
}
