import { EmailValid } from "../../../utils/validations";
import { dataChile } from "../../../utils";

export const formDireccion = (data) => {
  const [region] = dataChile?.regions?.filter(
    // eslint-disable-next-line
    ({ number }) => number == data?.region
  );
  return {
    direccion: {
      value: data?.direccion ?? ``,
      error: false,
      label: "Dirección",
      placeholder: "Escriba aquí",
      inputType: "TextInput",
      inputRequired: false,
    },
    region: {
      value: data?.region ?? null,
      error: false,
      label: "Region",
      placeholder: "Escriba aquí",
      inputType: "Select",
      items:
        dataChile?.regions?.map(({ name, number }) => ({
          label: name,
          value: number,
        })) ?? [],
      onChange: {
        name: "comuna",
        data: dataChile?.regions,
        filter: "number",
        index: "communes",
      },
    },
    comuna: {
      value: data?.comuna ?? null,
      error: false,
      label: "Comuna",
      placeholder: "Escriba aquí",
      inputType: "Select",
      isValue: true,
      items: region?.communes.map(({ name, identifier }) => ({
        label: name,
        value: identifier,
      })),
    },
  };
};

export const formPrincipal = (data) => {
  return {
    descripcion: {
      value: data?.descripcion ?? ``,
      error: false,
      label: "Descripción",
      placeholder: "Escriba aquí",
      inputType: "TextInput",
      inputRequired: true,
      multiline: true,
      rows: 4,
    },
    email: {
      value: data?.email ?? null,
      error: false,
      label: "Correo electrónico",
      placeholder: "Escriba aquí",
      inputType: "TextInput",
      rules: { ...EmailValid },
    },
    telefono: {
      value: data?.telefono ?? null,
      error: false,
      label: "Telefono",
      placeholder: "Escriba aquí",
      inputType: "TextInput",
      type: "number",
      rules: { min: 8, max: 9 },
    },
    celular: {
      value: data?.celular ?? null,
      error: false,
      label: "Celular",
      placeholder: "Escriba aquí",
      inputType: "TextInput",
      type: "number",
      rules: { min: 8, max: 9 },
    },
    facebook: {
      value: data?.facebook ?? null,
      error: false,
      label: "Facebook",
      placeholder: "Escriba aquí",
      inputType: "TextInput",
    },
    instragram: {
      value: data?.instragram ?? null,
      error: false,
      label: "Instragram",
      placeholder: "Escriba aquí",
      inputType: "TextInput",
    },
    tiktok: {
      value: data?.tiktok ?? null,
      error: false,
      label: "Tik Tok",
      placeholder: "Escriba aquí",
      inputType: "TextInput",
    },
    sitio: {
      value: data?.sitio ?? null,
      error: false,
      label: "Sitio Web",
      placeholder: "Escriba aquí",
      inputType: "TextInput",
    },
  };
};

export const formInfoUser = (data) => {
  return {
    name: {
      value: data?.name ?? ``,
      error: false,
      label: "Nombre",
      placeholder: "Escriba aquí",
      inputType: "TextInput",
      inputRequired: true,
    },
    email: {
      value: data?.email ?? null,
      error: false,
      label: "Correo electrónico",
      placeholder: "Escriba aquí",
      inputType: "TextInput",
      rules: { ...EmailValid },
    },
  };
};
