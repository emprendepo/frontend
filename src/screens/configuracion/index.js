import React from "react";
import Paper from "@material-ui/core/Paper";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import {
  Principal,
  Direccion,
  Logos,
  Suscripcion,
  InfoUser,
} from "./components";

const useStyles = makeStyles((theme) => ({
  root: {
    marginTop: 50,
  },
}));

export default function Configuracion() {
  const classes = useStyles();
  const [value, setValue] = React.useState(localStorage.getItem(`tabs`) ?? 0);
  const handleChange = (event, newValue) => {
    localStorage.setItem(`tabs`, newValue);
    setValue(newValue);
  };

  const handleComponent = (tab) =>
    ({
      0: <Principal />,
      1: <Direccion />,
      2: <Logos />,
      3: <Suscripcion />,
      4: <InfoUser />,
    }[tab] || 0);

  return (
    <Grid className={classes.root}>
      <Paper>
        <Tabs
          value={value}
          indicatorColor="primary"
          textColor="primary"
          onChange={handleChange}
          aria-label="disabled tabs example"
        >
          <Tab label="Información Pyme" />
          <Tab label="Dirección" />
          <Tab label="Logos" />
          <Tab label="Suscripción" />
          <Tab label="Información Usuario" />
        </Tabs>

        {handleComponent(value)}
      </Paper>
    </Grid>
  );
}
