import React, { useState, useEffect } from "react";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Modal from "@material-ui/core/Modal";
import Divider from "@material-ui/core/Divider";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import { withStyles } from "@material-ui/core/styles";
import Form from "../../components/form";
import { handleChangeForm } from "../../utils";
import { EmailValid, RutValid } from "../../utils/validations";
import CloseOutlinedIcon from "@material-ui/icons/CloseOutlined";
import { Loading } from "../../components";

function CrudModal({
  classes,
  onClose,
  modal,
  form,
  entity,
  update,
  create,
  errors,
  pay,
  isLoading = false,
}) {
  const [data, setData] = useState({});
  const [isValid, setIsValid] = useState(false);
  const [generalFormError, setGeneralFormError] = useState(false);
  const [formData, setFormData] = useState(form);
  const validateInput = { EmailValid, RutValid };

  const actionName = () =>
    ({
      edit: "Editar",
      validate: "Validar",
      add: "Agregar",
      pay: "Pagar",
    }[modal.type] || [`add`]);

  const onChangeForm = ({ name, value }) => {
    let newData = {};
    let nextForm = handleChangeForm(formData, name, value);
    value = nextForm[name].value;
    newData = { ...data, [nextForm[name].name]: value };
    setFormData(nextForm);
    setData(newData);
  };

  useEffect(() => {
    try {
      let data = formData.map((item, i) => {
        return {
          ...item,
          value:
            modal.type === `edit` || modal.type === `pay`
              ? modal.data[item.name]
              : "",
          validations: item.validations && validateInput[item.validations],
        };
      });
      setFormData(data);
    } catch (e) {
      setFormData(formData);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [modal]);

  useEffect(() => {
    setGeneralFormError("");
    let nextIsValid = true;
    Object.keys(formData).forEach((name) => {
      const { value, error, inputRequired, inputType } = formData[name];

      if (
        (inputRequired &&
          (!value || (inputType === "Select" && value === -1))) ||
        (error && error.length > 0)
      ) {
        nextIsValid = false;
      }
    });
    if (nextIsValid) {
      setGeneralFormError(false);
    }
    setIsValid(nextIsValid);
  }, [formData]);

  const accionSubbmit = (item) =>
    ({
      edit: () => update({ ...modal.data, ...data }),
      add: () => create(data),
      pay: () => pay({ ...modal.data, ...data }),
    }[item] || [`add`]);

  return (
    <Modal open={modal.show} disableBackdropClick disableEscapeKeyDown>
      <Paper className={classes.paper}>
        <Grid container direction="column">
          <Grid
            item
            className={classes.sections}
            container
            justify="space-between"
            alignItems="center"
          >
            <Typography variant="h4" className={classes.modalTitle}>
              {`${actionName()} ${entity === `servicio` ? `evento` : entity}`}
            </Typography>
            <CloseOutlinedIcon
              onClick={() => onClose()}
              className={classes.icon}
            />
          </Grid>
          <Divider />
          <Grid
            container
            className={classes.formContainer}
            justify="center"
            alignContent="center"
            spacing={1}
          >
            {isLoading ? (
              <Loading />
            ) : (
              <Form
                itemId={modal?.data?.id}
                formName={null}
                formSubName={null}
                form={formData}
                onChangeForm={onChangeForm}
                isValid={true}
                loading={isLoading}
                hideSubmitButton={true}
                formError={[]}
              />
            )}
            {errors &&
              errors.map((error, index) => {
                return (
                  <Grid item>
                    <Typography
                      align={`center`}
                      key={index}
                      variant={`body2`}
                      color="error"
                    >
                      {error}
                    </Typography>
                  </Grid>
                );
              })}
          </Grid>
          <Divider />
          {!isLoading && (
            <Grid
              item
              className={classes.sections}
              container
              justify="flex-end"
            >
              <Button
                type="button"
                className={classes.button}
                onClick={() => onClose()}
              >
                Cancelar
              </Button>
              <Button
                type="button"
                variant="contained"
                onClick={accionSubbmit(modal.type)}
                className={classes.continueButton}
                disabled={!isValid || generalFormError}
              >
                Confirmar
              </Button>
            </Grid>
          )}
        </Grid>
      </Paper>
    </Modal>
  );
}

const styles = (theme) => ({
  caption: {
    fontSize: `1.6rem`,
    lineHeight: `3rem`,
    color: `#042E48`,
    paddingBottom: theme.spacing(1),
  },
  paper: {
    position: `absolute`,
    width: `80%`,
    maxHeight: `100vh`,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing(1),
    top: `50%`,
    left: `50%`,
    transform: `translate(-50%, -50%)`,
    [theme.breakpoints.down(`sm`)]: {
      width: `100%`,
      overflowY: `scroll`,
      overflowX: `hidden`,
    },
  },
  formContainer: {
    margin: theme.spacing(1, 0),
    padding: theme.spacing(1),
    [theme.breakpoints.down(`sm`)]: {
      overflowY: `scroll`,
    },
  },
  button: {
    marginTop: `1em`,
    background: `none`,
    color: `#042E48`,
    marginLeft: theme.spacing(1),
    "&:hover": {
      color: `#042E48`,
      background: `none`,
    },
    [theme.breakpoints.down(`sm`)]: {
      width: `100%`,
    },
  },
  error: {
    color: `#f5222d`,
    fontSize: `0.8em`,
  },
  continueButton: {
    marginTop: `1em`,
    color: `#042E48`,
    marginLeft: theme.spacing(1),
    boxShadow: `0 2px 4px 0 rgba(0,0,0,0.40)`,
    "&:hover": {
      color: `#042E48`,
      backgroundColor: `#00D8BB`,
    },
    [theme.breakpoints.down(`sm`)]: {
      width: `100%`,
    },
  },
  icon: {
    paddingBottom: theme.spacing(1),
    "&:hover": {
      cursor: `pointer`,
    },
  },
  modalTitle: {
    padding: 10,
    [theme.breakpoints.down(`sm`)]: {
      fontSize: `2.5rem`,
      padding: 2,
      width: `90%`,
    },
  },
});
export default withStyles(styles)(CrudModal);
