import React, { useState, useEffect } from "react";
import { withRouter } from "react-router-dom";
import Grid from "@material-ui/core/Grid";
import { withStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import Modal from "antd/es/modal";
import API from "../../config/api";
import CrudModal from "./crudModal";
import TableComponent from "../../components/table";
import DeleteOutlineOutlinedIcon from "@material-ui/icons/DeleteOutlineOutlined";
import EditLocationOutlinedIcon from "@material-ui/icons/EditLocationOutlined";
import AddCircleOutlineOutlinedIcon from "@material-ui/icons/AddCircleOutlineOutlined";
import AttachMoneyIcon from "@material-ui/icons/AttachMoney";
import { formatRut } from "utils/rut";
import {
  ChangeStatusPedidos,
  DetalleVenta,
  StatusClientes,
  PrintDetalle,
} from "../../components/crud";
import { TramRounded } from "@material-ui/icons";
const { confirm } = Modal;

function Crud({
  classes,
  history,
  match: {
    params: { entity },
  },
}) {
  const isGeneric = ![`empresas`, `User`].includes(entity);
  const isAdd = ![`facturas`, `boletas`, `arriendos`].includes(entity);
  const isPay = ![`arriendos`].includes(entity);
  const isEdit = ![`facturas`, `boletas`, `arriendos`].includes(entity);
  const isEditBtn = ![`Pedidos`].includes(entity);
  const isLink = [`facturas`, `boletas`, `arriendos`].includes(entity);
  const isDelete = ![`facturas`, `boletas`, `arriendos`, `Pedidos`].includes(
    entity
  );
  const [table, setTable] = useState({
    dataSource: [],
    columns: [],
    pagination: {},
  });
  const [modal, setModal] = useState({ show: false, data: {}, type: `` });
  const [form, setForm] = useState([]);
  const [loading, setLoading] = useState(true);
  const [errors, setErrors] = useState([]);

  const Title = () =>
    ({
      autores: "Autores",
      librosEstado: "Estados",
      metodoDePago: "Metodos de Pagos",
    }[entity] || entity);

  const validateEspecial = (data) => {
    if (entity === `role`) {
      return ![`admin`, `ciudadano`, `chileatiende`, `instituciones`].includes(
        data.nombre.toLowerCase()
      );
    }
    return isDelete;
  };

  const renderColumns = (val, name) => {
    if (typeof val === `boolean`) {
      return val ? `Activo` : `Inactivo`;
    }
    if (name === "rut") {
      return formatRut(val);
    }
    if (
      name === "valor" ||
      name === "valor_total" ||
      name === "precio_con_iva" ||
      name === "precio_neto" ||
      name === "costo_iva"
    ) {
      return new Intl.NumberFormat(`es-CL`, {
        currency: `CLP`,
        style: `currency`,
      }).format(val);
    }
    if (Array.isArray(val)) {
      return val
        .map((value) => {
          if (name === `filtro_info`) return value.dato;
          return value.name || value.nombre || value.title || value.codigo;
        })
        .join(` - `);
    }
    if (typeof val === `object` && val !== null) {
      return (
        val.name ||
        val.nombre ||
        val.title ||
        val.nombres ||
        val.codigo ||
        val.folio
      );
    }
    return val;
  };
  const setColumns = (dataForm) => {
    let columns = dataForm
      .map(({ label, name }) => {
        return {
          title: label.replace(`_`, ` `),
          dataIndex: name.includes(`_id`) ? name.split(`_id`)[0] : name,
          key: name.includes(`_id`) ? name.split(`_id`)[0] : name,
          render: (val) => renderColumns(val, name),
        };
      })
      .filter((data) => data.dataIndex !== `contrasena`);

    if (entity === `Pedidos`) {
      columns = [
        {
          title: `Numero Pedido`,
          key: `cliente`,
          render: ({ id }) => id ?? "",
        },
        {
          title: `Cliente`,
          key: `cliente`,
          render: ({ boletas }) => boletas?.clientes?.nombres ?? "",
        },
        {
          title: `Metodo de Pago`,
          key: `metodo_de_pago`,
          render: ({ boletas }) => boletas?.metodo_de_pago?.nombre ?? "",
        },
        ...columns,
      ];
    }

    return [
      ...columns,
      {
        title: `Acciones`,
        render: (data) => {
          return (
            <>
              {isEditBtn && (
                <EditLocationOutlinedIcon
                  className={classes.icon}
                  onClick={() =>
                    !isEdit
                      ? history.push(
                          `/pyme/edit/${entity}/${data.folio || data.id}`
                        )
                      : setModal({ show: true, data, type: `edit` })
                  }
                />
              )}

              {entity === "Pedidos" && (
                <ChangeStatusPedidos callback={handleCallback} items={data} />
              )}
              {entity === "Pedidos" && (
                <StatusClientes callback={handleCallback} items={data} />
              )}
              {entity === "Pedidos" && (
                <DetalleVenta callback={handleCallback} items={data} />
              )}
              {entity === "Pedidos" && (
                <PrintDetalle callback={handleCallback} id={data?.id} />
              )}

              {!isPay && (
                <AttachMoneyIcon
                  className={classes.icon}
                  onClick={() => handlePay(data.id || data.folio)}
                />
              )}

              {validateEspecial(data) && (
                <DeleteOutlineOutlinedIcon
                  className={classes.icon}
                  onClick={() =>
                    confirm({
                      title: `¿Estas seguro que deseas eliminar "${
                        data.nombre ||
                        data.nombres ||
                        data.title ||
                        data.name ||
                        data.titulo ||
                        data.codigo
                      }"?`,
                      onOk() {
                        deleteEntity(data.id);
                      },
                    })
                  }
                />
              )}
            </>
          );
        },
      },
    ];
  };

  const setPagination = (data) => {
    return {
      total: data.total,
      perPage: data.per_page,
      currentPage: data.current_page,
    };
  };
  const getData = async (page, pageSize) => {
    let entityPage = `${entity}?page=1&perPage=20`;
    if (page && pageSize) {
      entityPage = `${entity}?page=${page}&perPage=${pageSize}`;
      const endpoints = isGeneric
        ? [API.getEntity(entityPage)]
        : [API.getCustomEntity(entityPage)];
      const [dataEntity] = await Promise.all(endpoints);
      setForm(dataEntity);
      setTable({
        columns: table.columns,
        dataSource: dataEntity.data.map((d) => {
          return { ...d, key: d.id };
        }),
        pagination: setPagination(dataEntity),
      });
    } else {
      const endpoints = isGeneric
        ? [API.getForm(entity), API.getEntity(entityPage)]
        : [API.getCustomForm(entity), API.getCustomEntity(entityPage)];
      const [dataForm, dataEntity] = await Promise.all(endpoints);
      const data = dataForm?.filter(({ isVisible = true }) => isVisible);
      if (data) {
        setForm(data);
        setTable({
          columns: setColumns(data),
          dataSource: dataEntity.data.map((d) => {
            return { ...d, key: d.id };
          }),
          pagination: setPagination(dataEntity),
        });
      }
    }
    setLoading(false);
  };

  const handleCallback = async () => {
    setLoading(TramRounded);
    setModal({ show: false });
    await getData();
    setLoading(false);
  };

  const create = async (body) => {
    setLoading(true);
    if (isGeneric) {
      await API.createEntity(entity, body)
        .then(() => {
          setModal({ show: false });
        })
        .catch((e) => {
          setErrors([e ?? "Hubo un Error"]);
          setLoading(false);
        });
    } else {
      await API.createCustomEntity(entity, body)
        .then((e) => {
          setModal({ show: false });
          setErrors([]);
        })
        .catch((e) => {
          setErrors([e ?? "Hubo un Error"]);
          setLoading(false);
        });
    }
    await getData();
    setLoading(false);
  };

  const pay = async ({ id, ...body }) => {
    await API.postEntityCustom(`arriendo/form/pay/${id}`, body);
    setModal({ show: false });
    await getData();
  };

  const update = async ({ id, ...body }) => {
    setLoading(true);
    if (isGeneric) {
      await API.updateEntity(id, entity, body).then(setModal({ show: false }));
    } else {
      await API.updateCustomEntity(id, entity, body).then(
        setModal({ show: false })
      );
    }
    await getData();
    setLoading(false);
  };

  const deleteEntity = async (id) => {
    setLoading(true);
    if (isGeneric) {
      await API.deleteEntity(id, entity);
    } else {
      await API.deleteCustomEntity(id, entity);
    }
    await getData();
    setLoading(false);
  };

  const handlePay = async (id) => {
    const { form, data } = await API.getEntityCustom(`arriendo/form/pay/${id}`);
    setForm(form);
    setModal({ show: true, data: { id, ...data }, type: `pay` });
  };

  useEffect(() => {
    if (isGeneric) {
      getData();
      setLoading(true);
    } else {
      switch (entity) {
        case `empresas`:
          getData();
          setLoading(true);
          break;
        case `User`:
          getData();
          setLoading(true);
          break;
        default:
          break;
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [entity, isGeneric]);
  return (
    <Grid
      container
      className={`content contrast-black-bg ${classes.root}`}
      spacing={0}
    >
      {modal.show && (
        <CrudModal
          modal={modal}
          errors={errors}
          onClose={() => {
            setErrors([]);
            setModal({ show: false });
          }}
          entity={entity}
          form={form}
          update={update}
          create={create}
          pay={pay}
          isLoading={loading}
        />
      )}

      <Grid item xs={12}>
        <Paper className={classes.paperContainer}>
          <Grid container className={classes.root}>
            <Grid item container xs={12} alignItems="center">
              <Grid item xs={12} md={9}>
                <Typography variant="h2" className={classes.modalTitle}>
                  Lista de {entity === `servicio` ? `evento` : Title()}
                </Typography>
              </Grid>
              <Grid item xs={12} md={3} container justify="flex-end">
                {isLink && !loading && (
                  <Button
                    variant="contained"
                    className={classes.button}
                    onClick={() => history.push(`/pyme/add/${entity}`)}
                  >
                    {`Agregar ${entity === `servicio` ? `evento` : Title()} `}
                    <AddCircleOutlineOutlinedIcon />
                  </Button>
                )}

                {isAdd && !loading && (
                  <Button
                    variant="contained"
                    className={classes.button}
                    onClick={() =>
                      setModal({ ...modal, show: true, type: `add` })
                    }
                  >
                    {`Agregar ${entity === `servicio` ? `evento` : Title()} `}
                    <AddCircleOutlineOutlinedIcon />
                  </Button>
                )}
              </Grid>
            </Grid>
            <Grid item xs={12}>
              <TableComponent
                loading={loading}
                pagination={table.pagination}
                dataSource={table.dataSource}
                columns={table.columns}
                getData={getData}
              />
            </Grid>
          </Grid>
        </Paper>
      </Grid>
    </Grid>
  );
}

const styles = (theme) => ({
  root: {
    padding: theme.spacing(2),
    [theme.breakpoints.down(`sm`)]: {
      padding: 0,
    },
  },
  paperContainer: {
    margin: theme.spacing(4),
    [theme.breakpoints.down(`sm`)]: {
      padding: `20px 8px 10px 8px !important`,
    },
  },
  icon: {
    color: `#00d6ba`,
    "&:hover": {
      pointer: `cursor`,
    },
  },
  button: {
    textDecoration: `none`,
    marginBottom: theme.spacing(2),
    [theme.breakpoints.down(`sm`)]: {},
  },
});

export default withRouter(withStyles(styles)(Crud));
