import React, { useState } from "react";
import clsx from "clsx";
import { makeStyles } from "@material-ui/core/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import Header from "./header";
import Sidebar from "./sidebar";
import { useAuth } from "../../context";
import { Switch, Route } from "react-router-dom";
const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
    transition: theme.transitions.create("margin", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    marginLeft: -drawerWidth,
  },
  contentShift: {
    transition: theme.transitions.create("margin", {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
    marginLeft: 0,
  },
  drawerHeader: {
    display: "flex",
    alignItems: "center",
    padding: theme.spacing(0, 1),
    ...theme.mixins.toolbar,
    justifyContent: "flex-end",
  },
}));

export default function Layout({ routes: routesProps = [] }) {
  const classes = useStyles();
  const [open, setOpen] = useState(true);
  const { user } = useAuth();

  const handleValidate = (user, validate) => {
    if (validate === "validate-suscribete") {
      return user?.suscripciones ? true : false;
    }
    if (validate === "validate-suscribete-pro") {
      return user?.suscripciones?.type === "2" ? true : false;
    }
    return true;
  };

  const routes = routesProps
    ?.map(({ validate, ...items }) => ({
      ...items,
      rules: validate,
      validate: handleValidate(user, validate),
    }))
    .filter(({ validate }) => validate);

  return (
    <div className={classes.root}>
      <CssBaseline />
      <Header open={open} onOpen={() => setOpen(true)} />
      <Sidebar routes={routes} open={open} onOpen={() => setOpen(false)} />
      <main
        className={clsx(classes.content, {
          [classes.contentShift]: open,
        })}
      >
        <Switch>
          {routes.map(({ exact = true, ...routeProps }) => (
            <Route {...routeProps} exact={exact} />
          ))}
        </Switch>
      </main>
    </div>
  );
}
