import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Typography } from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Chart from "react-apexcharts";
import { Table } from "antd";
import API from "../../config/api";
import { Loading } from "../../components";

const useStyles = makeStyles(({ breakpoints }) => ({
  paperContainer: {
    marginTop: 50,
    [breakpoints.down(`sm`)]: {
      padding: `20px 8px 10px 8px !important`,
    },
  },
}));
function Dashboard() {
  const classes = useStyles();
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState(false);
  const [data, setData] = useState({
    donuts: [],
    BasicBar: [],
    masVendidoProducto: [],
    masVendidoCategoria: [],
    clientes: [],
    ventas: [],
  });

  const state = ({ categories = [], data = [] }) => {
    return {
      options: {
        dataLabels: {
          enabled: true,
          formatter: function (val) {
            return (
              new Intl.NumberFormat(`es-CL`, {
                currency: `CLP`,
                style: `currency`,
              }).format(val) ?? 0
            );
          },
        },
        chart: {
          id: "basic-bar",
        },
        xaxis: {
          categories,
        },
      },
      series: [
        {
          name: "series-1",
          data,
        },
      ],
    };
  };

  const donuts = ({ series = [], labels = [] }) => {
    return {
      options: { labels },
      series,
    };
  };

  const handleTableCategoria = ({ masVendidoCategoria }) => {
    return {
      dataSource: [masVendidoCategoria],
      columns: [
        {
          title: "Identificador",
          dataIndex: "id",
          key: "id",
        },
        {
          title: "Nombre",
          dataIndex: "name",
          key: "name",
        },
        {
          title: "Descripción",
          dataIndex: "descripcion",
          key: "descripcion",
        },
      ],
    };
  };

  const handleTableProducto = ({ masVendidoProducto }) => {
    return {
      dataSource: [masVendidoProducto],
      columns: [
        {
          title: "Identificador",
          dataIndex: "id",
          key: "id",
        },
        {
          title: "Numero Serie",
          dataIndex: "numero_serie",
          key: "numero_serie",
        },
        {
          title: "Nombre",
          dataIndex: "nombre",
          key: "nombre",
        },
        {
          title: "Descripción",
          dataIndex: "descripcion",
          key: "descripcion",
        },
      ],
    };
  };

  const handleTableClientes = ({ clientes }) => {
    return {
      dataSource: clientes,
      columns: [
        {
          title: "Nombre",
          dataIndex: "nombres",
          key: "nombres",
        },
        {
          title: "Rut",
          dataIndex: "rut",
          key: "rut",
        },
        {
          title: "Email",
          dataIndex: "email",
          key: "email",
        },
        {
          title: "Telefono",
          dataIndex: "telefono",
          key: "telefono",
        },
      ],
    };
  };

  useEffect(() => {
    const getData = async () => {
      try {
        setIsLoading(true);
        const data = await API.getCustomEntity(`pyme/dashboard`);
        if (data?.view) {
          setData(data);
        } else {
          setError("Aún no tiene data ingresada.");
        }
        setIsLoading(false);
      } catch (e) {
        setError("Hubo un error comuniquese con admin@emprendepo.cl");
        setIsLoading(false);
      }
    };
    getData();
  }, []);

  if (isLoading) {
    return <Loading />;
  }

  if (error) {
    return (
      <Grid
        container
        justify="center"
        alignContent="center"
        className={classes.paperContainer}
      >
        <Typography variant="h4" align="center">
          {error}
        </Typography>
      </Grid>
    );
  }

  return (
    <Paper className={classes.paperContainer}>
      <Grid
        container
        justify="center"
        alignContent="center"
        className={classes.root}
      >
        <Grid item md style={{ margin: 10 }}>
          <Typography variant="h6" align="left">
            Metodos de pagos 10 ultimas ventas
          </Typography>
          <Chart type="donut" width="400" {...donuts(data?.donuts)} />
        </Grid>
        <Grid item md style={{ margin: 10 }}>
          <Typography variant="h6" align="left">
            total ultimas 10 ventas
          </Typography>
          <Chart type="bar" width="400" {...state(data?.BasicBar)} />
        </Grid>

        <Grid item md={12} style={{ margin: 10 }}>
          <Typography variant="h6" align="center">
            Categoria mas vendida
          </Typography>
          <Table {...handleTableCategoria(data)} />
        </Grid>
        <Grid item md={12} style={{ margin: 10 }}>
          <Typography variant="h6" align="center">
            Producto mas vendido
          </Typography>
          <Table {...handleTableProducto(data)} />
        </Grid>
        <Grid item md={12} style={{ margin: 10 }}>
          <Typography variant="h6" align="center">
            Ultimo 10 Cliente
          </Typography>
          <Table {...handleTableClientes(data)} />
        </Grid>
      </Grid>
    </Paper>
  );
}

export default Dashboard;
