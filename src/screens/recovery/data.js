import {
  ValidateMayusula,
  validateEqual,
  ValidateMinuscula,
  validateNumero,
} from "../../utils";

const validate = [
  {
    field: false,
    validateFunction: (val, current) => {
      return ValidateMayusula(current);
    },
    error: `La contraseña debe contener al menos una mayúscula.`,
  },
  {
    field: false,
    validateFunction: (val, current) => {
      return ValidateMinuscula(current);
    },
    error: `La contraseña debe contener al menos una minúscula.`,
  },
  {
    field: false,
    validateFunction: (val, current) => {
      return validateNumero(current);
    },
    error: `La contraseña debe contener al menos un número`,
  },
];

export const formRecovery = {
  password: {
    value: ``,
    error: false,
    label: `Contraseña`,
    placeholder: `Elija Contraseña`,
    inputType: `TextInput`,
    type: `password`,
    inputRequired: true,
    rules: {
      required: true,
      min: 7,
      max: 20,
      validations: [
        ...validate,
        {
          field: `validatePassword`,
          validationType: `equal`,
          validateFunction: (validatePassword, current) => {
            if (validatePassword === ``) {
              return true;
            }
            return validateEqual(validatePassword, current);
          },
          error: `Los password deben ser iguales`,
        },
      ],
    },
  },
  repassword: {
    value: ``,
    error: false,
    label: `Confirme Contraseña`,
    placeholder: `Elija Contraseña`,
    inputType: `TextInput`,
    type: `password`,
    inputRequired: true,
    rules: {
      required: true,
      min: 7,
      max: 20,
      validations: [
        {
          field: `password`,
          validationType: `equal`,
          validateFunction: (password, current) => {
            return validateEqual(password, current);
          },
          error: `Los password deben ser iguales`,
        },
        ...validate,
      ],
    },
  },
};
