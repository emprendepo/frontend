import React, { useState, useEffect } from "react";
import { Grid, Typography } from "@material-ui/core";
import Form from "components/form/";
import { formRecovery } from "./data";
import { handleChangeForm } from "utils";
import API from "config/api";
import { useAuth } from "context";

function Recovery({ location: { search } }) {
  const [form, setForm] = useState(formRecovery);
  const [loading, setLoading] = useState(false);
  const [isValid, setIsValid] = useState(false);
  const [generalFormError, setGeneralFormError] = useState(false);
  const { logIn } = useAuth();
  const onChangeForm = ({ name, value }) => {
    let nextForm = handleChangeForm(form, name, value);
    value = nextForm[name].value;
    setForm(nextForm);
  };

  const Auth = async () => {
    try {
      setGeneralFormError(``);
      setLoading(true);
      const {
        password: { value: password },
      } = form;
      const token = search.replace(`?token=`, ``);
      const { access_token } = await API.createCustomEntity(
        `public/user/validate/token`,
        { password, token }
      );
      logIn(access_token);
      setLoading(false);
    } catch (e) {
      setLoading(false);
      setGeneralFormError(e);
    }
  };

  useEffect(() => {
    let nextIsValid = true;
    Object.keys(form).forEach((name) => {
      const { value, error, inputRequired, inputType } = form[name];
      if (
        (inputRequired &&
          (!value || (inputType === "Select" && value === -1))) ||
        (error && error.length > 0)
      ) {
        nextIsValid = false;
      }
    });
    if (nextIsValid) {
      setGeneralFormError(false);
    }
    setIsValid(nextIsValid);
  }, [form]);

  return (
    <>
      <Grid container justify="center" alignContent="center">
        <Grid item container direction="column" md={6}>
          <Grid item>
            <Typography
              variant="h1"
              align="center"
              dangerouslySetInnerHTML={{
                __html: `Valida y Cambia tu Password`,
              }}
            ></Typography>
          </Grid>
          <Grid item>
            <Typography variant="h6" align="center">
              puedes validar y cambiar tu contraseña para comenzar a ocupar la
              plataforma.
            </Typography>
          </Grid>
          <Grid item>
            <Form
              form={form}
              onChangeForm={onChangeForm}
              submitForm={Auth}
              isValid={isValid}
              loading={loading}
              hideSubmitButton={false}
              formError={generalFormError}
            />
          </Grid>
        </Grid>
      </Grid>
    </>
  );
}

export default Recovery;
