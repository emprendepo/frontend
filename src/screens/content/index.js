import React from "react";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import { makeStyles } from "@material-ui/core";

const homeStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(2),
    [theme.breakpoints.down(`sm`)]: {
      padding: 0,
    },
  },
  paperContainer: {
    margin: theme.spacing(4),
    [theme.breakpoints.down(`sm`)]: {
      padding: `20px 8px 10px 8px !important`,
    },
  },
}));

export default function Content({ children }) {
  const classes = homeStyles();
  return (
    <Grid
      container
      className={`content contrast-black-bg ${classes.root}`}
      spacing={0}
    >
      <Grid item xs={12}>
        <Paper className={classes.paperContainer}>
          <Grid container className={classes.root}>
            {children}
          </Grid>
        </Paper>
      </Grid>
    </Grid>
  );
}
