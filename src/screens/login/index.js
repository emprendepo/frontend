import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import LogosWrapper from "./LogosWrapper";
import Form from "../../components/form/";
import { formLogin } from "./formLogin";
import { handleChangeForm } from "../../utils";
import { useAuth } from "../../context";
import API from "../../config/api";
import Forget from "../../components/forget";
const useStyles = makeStyles(({ spacing, fontSizes, palette }) => ({
  root: {
    height: `100vh`,
    backgroundColor: `white`,
  },
  login: {
    maxWidth: `600px`,
    width: `100%`,
  },
  button: {
    marginTop: spacing(3),
    marginBottom: spacing(3),
    width: `100%`,
  },
  linkWrapper: {
    textAlign: `center`,
    width: `100%`,
  },
  link: {
    fontSize: fontSizes[1],
    color: palette.secondary.main,
  },
  errorMessage: {
    paddingBottom: spacing(3),
  },
}));
function Login() {
  const classes = useStyles();
  const [form, setForm] = useState(formLogin);
  const [loading, setLoading] = useState(false);
  const [isValid, setIsValid] = useState(false);
  const [generalFormError, setGeneralFormError] = useState(false);
  const { logIn } = useAuth();

  const onChangeForm = ({ name, value }) => {
    let nextForm = handleChangeForm(form, name, value);
    value = nextForm[name].value;
    setForm(nextForm);
  };

  const Auth = async () => {
    const {
      email: { value: email },
      password: { value: password },
    } = form;

    try {
      setLoading(true);
      const body = { email, password };
      const { access_token } = await API.login(body);
      logIn(access_token);
    } catch (error) {
      setGeneralFormError(error);
      setLoading(false);
    }
  };

  useEffect(() => {
    setLoading(false);
    let nextIsValid = true;
    Object.keys(form).forEach((name) => {
      const { value, error, inputRequired, inputType } = form[name];
      if (
        (inputRequired &&
          (!value || (inputType === "Select" && value === -1))) ||
        (error && error.length > 0)
      ) {
        nextIsValid = false;
      }
    });
    if (nextIsValid) {
      setGeneralFormError(false);
    }
    setIsValid(nextIsValid);
  }, [form]);

  return (
    <Grid container className={classes.root}>
      <LogosWrapper />
      <Grid
        item
        md={6}
        direction="column"
        alignItems="center"
        justify="center"
        container
      >
        <Grid item className={classes.login}>
          <Form
            formName={"Iniciar Sesión"}
            formSubName={`"Revisa, edita y actualiza."`}
            form={form}
            onChangeForm={onChangeForm}
            submitForm={Auth}
            isValid={isValid}
            loading={loading}
            hideSubmitButton={false}
            formError={generalFormError}
          />
        </Grid>
        <Forget />
      </Grid>
    </Grid>
  );
}

export default Login;
