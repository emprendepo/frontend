import React from "react";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";

const useStyles = makeStyles(({ spacing, fontWeights, palette }) => ({
  root: {
    background: `linear-gradient(0deg, ${palette.primary.main}, ${palette.primary.light}  )`,
    color: `white`,
    paddingBottom: `140px`,
  },
  principalTitle: {
    fontSize: `16em`,
    fontFamily: `GobCL`,
    fontWeight: fontWeights[3],
    lineHeight: `200px`,
  },
  secondaryTitle: {
    fontSize: `40px`,
    letterSpacing: `-0.5px`,
    fontWeight: fontWeights[0],
    lineHeight: `50px`,
    fontFamily: `GobCL`,
  },
  terniaryTitle: {
    fontSize: `23px`,
    fontWeight: `100`,
    lineHeight: `initial`,
    marginTop: spacing(2),
    fontFamily: `GobCL`,
  },
  titleWrapper: {
    maxWidth: `38em`,
  },
  logos: {
    position: `absolute`,
    bottom: `32px`,
  },
  logoFibe: {
    marginRight: `16px`,
  },
}));

function LogosWrapper() {
  const classes = useStyles();
  return (
    <Grid
      className={classes.root}
      item
      md={6}
      container
      justify="center"
      alignItems="center"
    >
      <Grid container className={classes.titleWrapper}>
        <Typography className={classes.principalTitle}>CRM</Typography>
        <Typography className={classes.secondaryTitle}>
          Sistema de Gestion
        </Typography>
        <Typography className={classes.terniaryTitle}>by emprendePo</Typography>
      </Grid>
    </Grid>
  );
}

export default LogosWrapper;
