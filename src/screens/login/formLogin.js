import { EmailValid } from "../../utils/validations";

export const formLogin = {
  email: {
    value: ``,
    error: false,
    label: "Correo electrónico",
    placeholder: "Escriba aquí",
    inputType: "TextInput",
    inputRequired: true,
    rules: { ...EmailValid },
  },
  password: {
    value: ``,
    error: false,
    label: "Password",
    placeholder: "Escriba aquí",
    inputType: "TextInput",
    type: "password",
    inputRequired: true,
  },
};
