import React from "react";
import { Route, withRouter, Redirect } from "react-router-dom";
import appRoutes from "./appRoutes";
import Layout from "../screens/layout";
import { validateUrl } from "../utils";
import { Switch } from "react-router-dom";

function Routes({ user: { roles } }) {
  const { privado, admin } = appRoutes;

  return (
    <>
      <Switch>
        {[
          { url: `/pyme`, userType: `pyme`, routes: privado },
          { url: `/admin`, userType: `admin`, routes: admin },
        ]
          .filter(({ userType }) => roles?.includes(userType))
          .map(({ url, userType, routes: routeProps }) => (
            <Route
              path={url}
              render={() => <Layout userType={userType} routes={routeProps} />}
              key={`route-${userType}`}
            />
          ))}
        <Redirect to={validateUrl(roles)} />
      </Switch>
    </>
  );
}

export default withRouter(Routes);
