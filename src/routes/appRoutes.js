import Crud from "../screens/crud";
import Login from "../screens/login";
import Home from "../screens/home";
import Dashboard from "../screens/dashboard";
import AddBoleta from "../screens/boletas/add";
import EditBoleta from "../screens/boletas/edit";
import Configuracion from "../screens/configuracion";
import Recovery from "../screens/recovery";
import Informes from "screens/informes";

const privateRoutes = [
  {
    url: `/pyme/home`,
    path: `/pyme/home`,
    validate: `validate-suscribete`,
    key: "home-view",
    component: Dashboard,
    text: "Dashboard",
    exact: false,
    mostrar: true,
  },
  {
    url: `/pyme/crud/Pedidos`,
    path: `/pyme/crud/:entity`,
    key: "Pedidos-view",
    component: Crud,
    text: "Pedidos",
    exact: false,
    mostrar: true,
  },
  {
    url: `/pyme/informes`,
    path: `/pyme/informes`,
    key: "informes-view",
    component: Informes,
    text: "Informes",
    exact: false,
    mostrar: true,
    validate: `validate-suscribete-pro`,
  },
  {
    url: `/pyme/crud/Clientes`,
    path: `/pyme/crud/:entity`,
    validate: `validate-suscribete`,
    key: "clientes-view",
    component: Crud,
    text: "Clientes",
    exact: false,
    mostrar: true,
  },
  {
    url: `/pyme/crud/Proveedores`,
    path: `/pyme/crud/:entity`,
    validate: `validate-suscribete`,
    key: "proveedores-view",
    component: Crud,
    text: "Proveedores",
    exact: false,
    mostrar: true,
  },
  {
    url: `/pyme/crud/Categoria`,
    path: `/pyme/crud/:entity`,
    key: "Categoria-view",
    component: Crud,
    text: "Categoria",
    exact: false,
    mostrar: true,
  },
  {
    url: `/pyme/crud/Productos`,
    path: `/pyme/crud/:entity`,
    key: "productos-view",
    component: Crud,
    text: "Productos",
    exact: false,
    mostrar: true,
  },
  {
    url: `/pyme/crud/metodoDePago`,
    path: `/pyme/crud/:entity`,
    validate: `validate-suscribete`,
    component: Crud,
    key: "metododepago-view",
    text: "Metodos de Pago",
    exact: false,
    mostrar: true,
  },
  {
    url: `/pyme/crud/boletas`,
    path: `/pyme/crud/:entity`,
    component: Crud,
    key: "boletas-view",
    text: "Boletas",
    exact: false,
    mostrar: true,
  },
  {
    url: `/pyme/add/boletas`,
    path: `/pyme/add/boletas`,
    key: "addboletas-view",
    component: AddBoleta,
    exact: true,
    mostrar: false,
  },
  {
    url: `/pyme/edit/boletas`,
    path: `/pyme/edit/boletas/:id`,
    key: "editboletas-view",
    component: EditBoleta,
    exact: true,
    mostrar: false,
  },
  {
    url: `/pyme/configuracion`,
    path: `/pyme/configuracion`,
    component: Configuracion,
    key: "configuracion-view",
    text: "Configuración",
    exact: false,
    mostrar: true,
  },
];

export const sharedRoutes = [
  {
    path: ["/", "/home"],
    key: "home-view",
    component: Login,
    exact: true,
    privado: false,
  },
  {
    path: ["/other"],
    key: "home-view",
    component: Home,
    exact: true,
  },
  {
    url: `/change/password`,
    path: "/change/password",
    key: "recovery-view",
    search: "?token=search-string",
    component: Recovery,
    exact: false,
  },
];

export const privateAdmin = [
  {
    url: `/admin/crud/empresas`,
    path: `/admin/crud/:entity`,
    key: "admin-view",
    component: Crud,
    text: "Empresas",
    exact: false,
    mostrar: true,
  },
  {
    url: `/admin/crud/User`,
    path: `/admin/crud/:entity`,
    key: "admin-view",
    component: Crud,
    text: "Users Crm",
    exact: true,
    mostrar: true,
  },
];

export const appRoutes = {
  publico: [...sharedRoutes],
  privado: [...privateRoutes],
  admin: [...privateAdmin],
};

export default appRoutes;
