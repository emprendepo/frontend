import React from "react";
import { Route, withRouter } from "react-router-dom";
import appRoutes from "./appRoutes";
import { Switch } from "react-router-dom";

function Public() {
  const { publico } = appRoutes;

  return (
    <>
      <Switch>
        {publico.map(({ exact = true, ...routeProps }) => (
          <Route {...routeProps} exact={exact} />
        ))}
      </Switch>
    </>
  );
}

export default withRouter(Public);
