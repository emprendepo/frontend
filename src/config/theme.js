import { createMuiTheme } from "@material-ui/core/styles";

const fontWeights = [300, 400, 500, 700];

export const colors = {
  primary: { main: `#2000e9`, light: `#4a25d6`, dark: `#5e39c2` },
  secondary: { main: `#0000df`, light: `#0d00e4` },
  terniary: { main: `#212121`, light: `#E3E3E3` },
  other: {
    pink: `#FF3B5D`,
    darkPuple: `#383368`,
    verde: `#C5DF5B`,
    verdeLight: `#5BDFDB`,
    lightblue: `#53B3E6`,
    mainBlue: `#548DD8`,
    categoriasNvo: `#D3D8F1`,
    orange: `#FD9727`,
    purple: `#6058B5`,
    letra: `#2E384D`,
    divider: `#7468BE`,
    borderLigth: `#CFD8FC`,
    grey: `#8798AD`,
    lineTable: `rgba(46, 91, 255, 0.1)`,
    border: `#CFD8FC`,
    contrastText: `#ecd54c`,
    green: `#9CD91B`,
  },
  grayRgba: [
    `rgba(224, 224, 224, 1)`, // 0
    `rgba(0, 111, 179, 0.08)`, // 1
    `rgba(80,227,194,0)`, // 2
    `rgba(0, 0, 0, 0.08)`, //3
    `rgba(0, 0, 0, 0.12)`, //4
    `rgba(0, 0, 0, 0.14)`, //5
    `rgba(0, 0, 0, 0.26)`, //6
    `rgba(0, 0, 0, 0.38)`, //7
    `rgba(0,0,0,0.50)`, //8
    `rgba(0, 0, 0, 0.54)`, //9
  ],
  gray: [`#F6F7FB`, `#D8D8D8`, `#979797`],
  error: { main: `#E02020` },
  success: { main: `#328A0E` },
};
const fontSizes = [
  18, //0 H5
  20, //1 H4
  22, //2 H3
  30, //3 H2
  40, //4 H1
];
const familyFont = [`Helvetica`, `Roboto`, `sans-serif`].join(`,`);

const theme = createMuiTheme({
  palette: {
    other: colors.other,
    primary: colors.primary,
    secondary: colors.secondary,
    terniary: colors.terniary,
    black: colors.primary.black,
    error: colors.error,
    gray: { main: `#AAAEB7`, dark: `#73787e` },
    success: colors.success,
    text: {
      primary: colors.other.letra,
      secondary: colors.primary.main,
      disabled: colors.grayRgba[7],
      hint: colors.grayRgba[7],
    },
    background: {
      default: colors.gray[0],
    },
    action: {
      active: colors.grayRgba[9],
      hover: colors.grayRgba[3],
      hoverOpacity: 0.08,
      selected: colors.grayRgba[5],
      disabled: colors.grayRgba[6],
      disabledBackground: colors.grayRgba[4],
    },
    type: `light`,
  },
  fontSizes,
  fontWeights,
});
theme.typography = {
  ...theme.typography,
  htmlFontSize: 10,
  fontSize: fontSizes[1],
  fontFamily: familyFont,
  h1: {
    fontSize: fontSizes[4],
    fontWeight: fontWeights[2],
    lineHeight: `4.5rem`,
    letterSpacing: -0.21,
    fontFamily: familyFont,
  },
  h2: {
    fontSize: fontSizes[3],
    fontWeight: fontWeights[2],
    lineHeight: `4.5rem`,
    letterSpacing: -0.21,
    fontFamily: familyFont,
  },
  h5: {
    fontSize: fontSizes[1],
    fontWeight: fontWeights[1],
    lineHeight: `2.0rem`,
    opacity: 0.8,
    fontFamily: familyFont,
  },
  body2: {
    fontSize: fontSizes[1],
    fontWeight: fontWeights[0],
    fontFamily: familyFont,
  },
};
theme.overrides = {};
export default theme;
