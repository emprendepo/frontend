import axios from "axios";
axios.defaults.headers.common["Access-Control-Allow-Origin"] = "*";
axios.defaults.headers.post[`Content-Type`] = `application/json`;
axios.defaults.baseURL = process.env.REACT_APP_API_URL;
axios.defaults.headers.common["Authorization"] =
  localStorage.getItem("token") || null;

const API = {};
const axiosInstance = axios.create();
const axiosInstanceDownloadPdf = axios.create();

axiosInstance.interceptors.response.use(
  (response) => {
    const {
      status = 200,
      data: { data, message = "Hubo un error", code = 200 },
    } = response;
    if (status !== 200) {
      if (code === 401) {
        window.location = `/`;
      } else {
        return Promise.reject({ message });
      }
    } else {
      return data;
    }
  },
  function ({ response: { data } }) {
    const { data: dataResponse } = data ?? {};
    return Promise.reject(dataResponse?.message ?? "Hubo un error");
  }
);

API.me = () => axiosInstance.get(`auth/me`);
API.login = (body) => axiosInstance.post(`auth/login`, { ...body });
//Entity
API.getEntity = (entity) => axiosInstance.get(`/entidad/${entity}`);
API.getEntityList = (entity) => axiosInstance.get(`/entidad/${entity}/list`);
API.updateEntity = (id, entity, body) =>
  axiosInstance.put(`entidad/${entity}/${id}`, { ...body });
API.createEntity = (entity, body) =>
  axiosInstance.post(`entidad/${entity}`, { ...body });
API.getForm = (entity) => axiosInstance.get(`/entidad/${entity}/form`);
API.deleteEntity = (id, entity) =>
  axiosInstance.delete(`entidad/${entity}/${id}`);

API.deleteCustomEntity = (id, url) => axiosInstance.delete(`${url}/${id}`);

API.getEntityCustom = (url) => axiosInstance.get(`${url}`);

API.postEntityCustom = (url, body) => axiosInstance.post(`${url}`, { ...body });
API.postUploadFile = (entity = `admin/file`, body) => axios.post(entity, body);
//Custom Entity
API.getCustomEntity = (entity) => axiosInstance.get(`/${entity}`);
API.getCustomForm = (entity) => axiosInstance.get(`/${entity}/form`);
API.getCustomEntityList = (entity) => axiosInstance.get(`/${entity}/list`);
API.updateCustomEntity = (id, entity, body) =>
  axiosInstance.put(`${entity}/${id}`, { ...body });
API.createCustomEntity = (entity, body) =>
  axiosInstance.post(`${entity}`, { ...body });

API.download = (url, nombreDocumento = `postulacion`, type = `pdf`, body) => {
  return axiosInstanceDownloadPdf
    .post(url, body, { responseType: "arraybuffer" })
    .then((response) => {
      let mimeType = null;
      if (type === `xlsx`) {
        mimeType = `vnd.ms-excel`;
      } else if (type === `pdf`) {
        mimeType = `pdf`;
      }
      const blob = new Blob([response.data], {
        type: `application/${mimeType}`,
      });
      const link = document.createElement(`a`);
      link.href = window.URL.createObjectURL(blob);
      link.download = `${nombreDocumento}.${type}`;
      link.click();
    })
    .catch((error) => {
      console.log(error);
    });
};

export default API;
