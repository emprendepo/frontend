import React from "react";
import { ThemeProvider } from "@material-ui/core/styles";
import theme from "./config/theme";
import Private from "./routes/private";
import Public from "./routes/public";
import { BrowserRouter } from "react-router-dom";
import { createBrowserHistory } from "history";
import { useAuth } from "./context";
const customHistory = createBrowserHistory();
function App() {
  const { user } = useAuth();
  return (
    <BrowserRouter history={customHistory}>
      <ThemeProvider theme={theme}>
        {!user ? <Public /> : <Private user={user} />}
      </ThemeProvider>
    </BrowserRouter>
  );
}

export default App;
